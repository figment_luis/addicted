<html><head><title>Antara</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><table align="center" width="600" height="400" border="0" cellpadding="0" cellspacing="0" style="font-family:Trebuchet MS, Arial, Helvetica, sans-serif;"><tr><td colspan="3" width="600" height="65"><img src="http://antara.kinetiq.mx/img/Addicted.png" style="display:none;" width="600" height="65" alt=""></td></tr><tr><td colspan="3" width="600" height="10"></td></tr><tr><td rowspan="2" width="10" height="325"></td><td width="580" height="315" align="left" valign="top">
<h2>TÉRMINOS, CONDICIONES DE USO Y POLÍTICA DE PRIVACIDAD</h2>
<p>Responsable de la protección de sus datos personales <br />
	Grupo Integral de Desarrollo Inmobiliario S de RL de CV (Antara), con domicilio en Av. Ejército Nacional 843-B, Col. Granada, Ciudad de México, Delegación Miguel Hidalgo, CP 11520, es responsable del tratamiento de sus datos personales. <br />
	<strong>Cómo contactarnos: </strong>Físicamente, por correo electrónico o vía telefónica.<br />
	<strong>Oficina de privacidad:</strong> Módulo de Conserjería de Antara (Concierge) <br />
	<strong>Domicilio:</strong> Centro Comercial Antara con dirección Av. Ejército Nacional 843-B, Col. Granada, Ciudad de México, Delegación Miguel Hidalgo, CP 11520<br />
	<strong>Correo electrónico:</strong> avisoprivacidad@antara.com.mx<br />
	<strong>Teléfono:</strong> 4593 8870</p>
<h2>¿Para qué fines recabamos y utilizamos sus datos personales?</h2>
<p><strong>Los datos personales que recopilamos de los usuarios que se registran y acceden a nuestros servicios en internet, por teléfono y en Concierge, los destinamos únicamente a los siguientes propósitos:</strong><br />
	(i) Fines de identificación y de verificación, (ii) contacto, (iii) informarle sobre su estatus dentro del programa de lealtad addicted, (iv) registro como usuarios de nuestros boletines electrónicos (newsletters), (v) promoción de nuestros servicios, productos y/o servicios de nuestros locatarios, así como de aquéllas empresas aliadas, (vi) identificar su historial de compras, (vii) ofrecerle participar en eventos y promociones organizadas por Antara. En la recolección y tratamiento de datos personales que usted nos proporcione, cumplimos todos los principios que marca la Ley (artículo 6): licitud, calidad, consentimiento, información, finalidad, lealtad, proporcionalidad y responsabilidad.</p>
<h2>¿Qué datos personales obtenemos y de dónde? </h2>
<p>Los datos personales que usted libre y voluntariamente proporcione a Antara a través de nuestros sitios <a href="http://www.antara.com.mx">www.antara.com.mx</a> y <a href="http://www.addicted.com.mx">www.addicted.com.mx</a>, mediante la utilización de nuestros servicios en Concierge y por vía telefónica en el Centro de Atención a Clientes, podrán incluir de manera enunciativa más no limitativa: (i) datos personales en general como son su nombre, domicilio personal, fecha de nacimiento, género, dirección de correo electrónico (e-mail), números telefónicos, estado civil, números de membrecía de programas asociados a nuestro programa de lealtad, así como (ii) laborales como son área de interés y curriculum vitae en el caso de que aplique a nuestra bolsa de trabajo.  Asimismo, podremos recabar información personal relacionada con sus preferencias en el consumo de nuestros servicios, productos y/o servicios de nuestros locatarios. <br />
	En el momento que usted se registra en nuestros sitios <a href="http://www.antara.com.mx">www.antara.com.mx</a> y <a href="http://www.addicted.com.mx">www.addicted.com.mx</a>, todos sus datos personales, son incorporados a nuestra base datos, utilizándose únicamente para los  propósitos anteriormente mencionados.<br />
	Usted manifiesta tácitamente que sus datos personales sean tratados de acuerdo a los términos y condiciones de este Aviso de Privacidad.</p>
<h2>¿Cómo puede limitar el uso o divulgación de sus datos personales? </h2>
<p>Usted puede dejar de  ser contactado por teléfono fijo o celular siguiendo los siguientes pasos: <br />
	Proceso en Concierge</p>
<ol>
	<li>Diríjase al módulo de Concierte</li>
	<li>Una vez en el módulo, indique al Concierge que desea dejar de ser contactado vía telefonía fija y/o celular</li>
	<li>Acredite su identidad mostrando su tarjeta del programa de lealtad addicted y una identificación oficial</li>
	<li>El Concierge hará las modificaciones pertinentes en el sistema y al cabo de 3 días hábiles dejará de ser contactado mediante el o los medios especificados</li>
	<li>Si Usted lo desea puede recibir un email de confirmación de que la modificación solicitada fue llevada a cabo, para lo cual sólo deberá indicar a que cuenta de correo desea recibir el aviso</li>
</ol>
<p>Proceso vía electrónica</p>
<ol>
	<li>Envíe un email a <a href="mailto:avisoprivacidad@antara.com.mx">avisoprivacidad@antara.com.mx</a> desde la cuenta de correo con que está suscrito al programa de lealtad addicted</li>
	<li>Incluya en el cuerpo del email la siguiente leyenda y el número telefónico en el cual no desea ser contactado: &quot;No deseo ser contactado vía telefonía móvil y/o celular en el/los  teléfono(s) NUMERO_TELEFONICO&quot;, en el &quot;Motivo del email&quot;, incluya la siguiente leyenda &quot;Baja de datos de contacto telefónico fijo y/o celular&quot; </li>
	<li>El departamento responsable  en Antara de mantener la privacidad de sus datos realizará las modificaciones pertinentes en el sistema y al cabo de 3 días hábiles dejará de ser contactado mediante el o los medios especificados</li>
	<li>Usted recibirá un mensaje de confirmación de que la modificación solicitada fue llevada a cabo, en la cuenta de correo electrónico desde la que hizo la solicitud</li>
</ol>
<p>Para dejar de recibir correos electrónicos  sólo debe dar click en la leyenda &quot;Para no recibir más nuestros e-mails, haz click aquí.&quot;, la cual se ubica en la parte inferior de todos nuestros mensajes de correo electrónico. </p>
<h2>¿Cómo acceder o rectificar sus datos personales o cancelar u oponerse a su uso? </h2>
<p>Usted tiene derecho de acceder a sus datos personales que poseemos y a los detalles del tratamiento de los mismos, así como a rectificarlos en caso de ser inexactos o incompletos; cancelarlos cuando considere que no se requieren para alguna de las finalidades señaladas en el presente aviso de privacidad, estén siendo utilizados para finalidades no consentidas o haya finalizado la relación contractual o de servicio, o bien, oponerse al tratamiento de los mismos para  fines específicos. <br />
	Los mecanismos que se han implementado para el ejercicio de dichos derechos son a través de la presentación de la solicitud respectiva en: <br />
	Proceso en Concierge</p>
<ol>
	<li>Diríjase al módulo de Concierte</li>
	<li>Una vez en el módulo, indique al Concierge que desea realizar alguna corrección o modificación a sus datos personales</li>
	<li>Acredite su identidad mostrando su tarjeta del programa de lealtad addicted y una identificación oficial</li>
	<li>Una vez acreditada su identidad, el Concierge tomará nota de las modificaciones a los datos que Usted solicite y al cabo de 3 días hábiles estos se verán reflejados en el sistema</li>
	<li>Si Usted lo desea puede recibir un email de confirmación de que la modificación solicitada fue llevada a cabo, para lo cual sólo deberá indicar a que cuenta de correo desea recibir el aviso</li>
</ol>
<p>Proceso vía electrónica</p>
<ol>
	<li>Envíe un email a <a href="mailto:avisoprivacidad@antara.com.mx">avisoprivacidad@antara.com.mx</a> desde la cuenta de correo con que está suscrito al programa de lealtad addicted</li>
	<li>Incluya en el cuerpo del email la indicación de los datos que solicita corregir&quot;, en el Motivo del email, incluya la siguiente leyenda &quot;Actualización o corrección de datos personales&quot;</li>
	<li>El departamento responsable  en Antara de mantener la privacidad de sus datos realizará las modificaciones pertinentes en el sistema y al cabo de 3 días hábiles dejará de ser contactado mediante el o los medios especificados</li>
</ol>
<p>Usted recibirá un mensaje de confirmación de que la modificación solicitada fue llevada a cabo, en la cuenta de correo electrónico desde la que hizo la solicitud</p>
<h2>¿Cómo puede revocar su consentimiento para el tratamiento de sus datos? </h2>
<p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, a fin de que dejemos de hacer uso de los mismos. Para ello, es necesario que presente su petición en. </p>
<p>Proceso en Concierge</p>
<ol>
	<li>Diríjase al módulo de Concierte</li>
	<li>Una vez en el módulo, indique al Concierge que desea dar de baja sus datos del programa de lealtad addicted</li>
	<li>Acredite su identidad mostrando su tarjeta del programa de lealtad addicted y una identificación oficial</li>
	<li>La tarjeta addicted será retenida por el Concierge y se le solicitará revisar y firmar la carta de renuncia a los puntos addicted que tenga acumulados</li>
	<li>Una vez recibida la carta de renuncia a los puntos addicted y su tarjeta addicted el Concierge hará las modificaciones pertinentes en el sistema y al cabo de 5 días hábiles sus datos quedarán eliminados del sistema por completo</li>
</ol>
<h2>Si Usted lo desea puede recibir un último email de confirmación de que la eliminación de los datos  solicitada fue llevada a cabo, para lo cual sólo deberá indicar a que cuenta de correo desea recibir el aviso.</h2>
<h2>Modificaciones al aviso de privacidad </h2>
<p>Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, para la atención de novedades legislativas, políticas internas o nuevos requerimientos para la prestación u ofrecimiento de nuestros servicios, productos y/o servicios de nuestros locatarios. <br />
	Estas modificaciones estarán disponibles al público a través de los siguientes medios: (i) Para consulta en el módulo de Concierge; (ii) en nuestros sitios <a href="http://www.antara.com.mx">www.antara.com.mx</a> y www.addicted.com.mx [sección aviso de privacidad]; (iii) o en link incluido en el pié de página de nuestros Newsletters.<br />
	¿Qué medidas de seguridad y control utilizamos para la protección de sus datos personales? <br />
	<br />
	Antara tiene implementadas medidas de seguridad administrativas, técnicas y físicas para proteger sus datos personales, mismas que igualmente exigimos sean cumplidas por los proveedores de servicios que contratamos, inclusive tratándose de servicios que prestan las empresas subsidiarias o afiliadas a Antara.</p>
<h2><br />
	Toda la información agregada y datos personales que se obtengan de usted a través del uso de nuestros sitios <a href="http://www.antara.com.mx">www.antara.com.mx</a> y www.addicted.com.mx, constituirá una base de datos propiedad de Antara, información que se almacena para protegerla y evitar su pérdida, uso indebido, o alteración. No obstante lo anterior, Antara en ninguna forma garantiza su seguridad, ni que la misma pueda ser interceptada, alterada o sustraída por terceros. </h2>
<h2>Uso de cookies y web beacons </h2>
<p>Las cookies son archivos de texto que son descargados automáticamente y almacenados en el disco duro del equipo de cómputo del usuario al navegar en una página de Internet específica, que permiten recordar al servidor de Internet algunos datos sobre este usuario, entre ellos, sus  preferencias para la visualización de las páginas en ese servidor, nombre y contraseña.<br />
	Por su parte, las web beacons son imágenes insertadas en una página de Internet o correo electrónico, que puede ser utilizado para monitorear el comportamiento de un visitante, como almacenar información sobre la dirección IP del usuario, duración del tiempo de interacción en dicha página y el tipo de navegador utilizado, entre otros.<br />
	Le informamos que utilizamos cookies y web beacons para obtener información personal de usted, como la siguiente: <br />
	Su tipo de navegador y sistema operativo. <br />
	Las páginas de Internet que visita. <br />
	Los vínculos que sigue. <br />
	La dirección IP. <br />
	El sitio que visitó antes de entrar al nuestro. <br />
	Antara puede utilizar estas cookies y otras tecnologías  para confirmar su identificación al tener acceso a nuestros sitios, con el solo propósito de otorgarle un servicio personalizado debido a que nos permite determinar sus preferencias mediante el registro de su uso en nuestros sitios por parte de los usuarios y de esta manera identificar sus hábitos y preferencias para ofrecerle nuestros servicios, productos y/o servicios de nuestros locatarios. Mediante el uso de las &quot;cookies&quot;, no recabamos datos personales de los usuarios en términos de la Ley ¿Ante quién puede presentar sus quejas y denuncias por el tratamiento indebido de sus datos  personales? <br />
	Si usted considera que su derecho de protección de datos personales ha sido lesionado por alguna conducta de nuestros empleados o de nuestras actuaciones o respuestas, presume que en el tratamiento de sus datos personales existe alguna violación a las disposiciones previstas en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, podrá interponer la queja o denuncia correspondiente ante el IFAI, para mayor información visite www.ifai.org.mx. <br />
	<strong>Fecha de última actualización de este Aviso de Privacidad:15/01/2013.</strong></p></td><td rowspan="2" width="10" height="325"></td></tr><tr><td width="580" height="10"></td></tr></table></body></html>