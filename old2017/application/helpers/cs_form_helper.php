<?php

if(!function_exists('setSelect'))
{
	function setSelect($name,$data,$selected='',$params = array(),$extra = '')
	{
		$res_id = array_key_exists('id', $params) ? ' id="'.$params['id'].'"':'';
		
		$res_visible = array_key_exists('visible', $params) ? $params['visible'] ? '':' style="display:none;"':'';
		
		$res_class = array_key_exists('class', $params) ? ' class="'.$params['class'].'"':'';
		
		$res_title = array_key_exists('label', $params) ? ' title="'.$params['label'].'"':'';
		
		$res_placeholder = array_key_exists('placeholder', $params) ? $params['placeholder']:'Seleccione';
				
		$arr_assoc = array_key_exists('arr_assoc', $params) ? $params['arr_assoc']:false;
		
		$res_extra = $extra != '' ? ' '.$extra.' ':'';
		
		$res = '<select name="'.$name.'"'.$res_id.$res_class.$res_visible.$res_title.$res_extra.'>'."\n";
		$res .= '<option>'.$res_placeholder.'</option>'."\n";
		if(!$arr_assoc)
		{
			foreach ($data as &$val)
			{
				$sel = $val == $selected ? ' selected="selected"':'';
				$res .= '<option'.$sel.'>';
				$res .= $val;
				$res .= '</option>'."\n";
			}
		}
		else
		{
			foreach ($data as $val => $labl)
			{
				$sel = $val == $selected ? ' selected="selected"':'';
				$res .= '<option value="'.$val.'"'.$sel.'>';
				$res .= $labl;
				$res .= '</option>'."\n";
			}
		}
		$res .= '</select>'."\n";
		return $res;
	}
}

if(!function_exists('estadosArray'))
{
	function estadosArray()
	{
		$estados = array(	'Aguascalientes'=>'Aguascalientes',
							'Baja California'=>'Baja California',
							'Baja California Sur'=>'Baja California Sur',
							'Campeche'=>'Campeche',
							'Chiapas'=>'Chiapas',
							'Chihuahua'=>'Chihuahua',
							'Coahuila de Zaragoza'=>'Coahuila',
							'Colima'=>'Colima',
							'Distrito Federal'=>'Distrito Federal',
							'Durango'=>'Durango',
							'Guanajuato'=>'Guanajuato',
							'Guerrero'=>'Guerrero',
							'Hidalgo'=>'Hidalgo',
							'Jalisco'=>'Jalisco',
							'Mexico'=>'México',
							'Michoacan de Ocampo'=>'Michoacán',
							'Morelos'=>'Morelos',
							'Nayarit'=>'Nayarit',
							'Nuevo Leon'=>'Nuevo León',
							'Oaxaca'=>'Oaxaca',
							'Puebla'=>'Puebla',
							'Queretaro'=>'Querétaro',
							'Quintana Roo'=>'Quintana Roo',
							'San Luis Potosí'=>'San Luis Potosí',
							'Sinaloa'=>'Sinaloa',
							'Sonora'=>'Sonora',
							'Tabasco'=>'Tabasco',
							'Tamaulipas'=>'Tamaulipas',
							'Tlaxcala'=>'Tlaxcala',
							'Veracruz de Ignacio de la Llave'=>'Veracruz',
							'Yucatan'=>'Yucatán',
							'Zacatecas'=>'Zacatecas');
		return $estados;
	}
}

?>