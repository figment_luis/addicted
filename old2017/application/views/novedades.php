<div class="contenedor">
	<!--Contenido -->
	<div class="contenidos">

		<!--Slide del TOP -->
		<div id="slideshow" class="pics" style="position: relative; height:416px;">
			<img width="961" height="416" src="imagenes/top-descubre.jpg" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 3; opacity: 1; width: 961px; height: 416px;">
			<img width="961" height="416" src="imagenes/top-contacto.jpg" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 3; opacity: 0; width: 961px; height: 416px;">
			<img width="961" height="416" src="imagenes/top-premios.jpg" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 3; opacity: 0; width: 961px; height: 416px;">
		</div>

		<div style="width: 250px; height: 330px; position: absolute; top: 430px; right: 10px; border-color:#000; border-left:thin; border-left-style:solid; text-align:right; z-index: -1;">
			<img src="imagenes/concierge.jpg" />
			<p style="font-size:18px; margin-top: 5px;">
				"Visita nuestro concierge"
			</p>
			<p class="color" style="font-size:10px; margin-top: 5px;">
				Esta úbicado en planta baja
			</p>
			<div style="font-size:1.5em;  margin-top: 8px;"><img src="imagenes/telefono.png" width="22" height="22" />4593-8870
			</div>
		</div>

		<div id="main">
			<div id="wrapper" style="min-height:600px;">
				<p class="primary_color" style="font-size:25px;">
					<br>
				</p>
				<div id="n_selected" style="display:none;">
					<div id="n_img"></div><div id="n_title"></div><div id="n_txt"></div><div style="clear:both;"></div>
					<div id="n_share_l">
						<span id="n_url" class="boton">Ver tienda</span><iframe src="https://www.facebook.com/plugins/like.php?href=<?= current_url() ?>" scrolling="no" frameborder="0" style="border:none;width:400px;height:29px"></iframe><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
						<script>
							! function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0];
								if (!d.getElementById(id)) {
									js = d.createElement(s);
									js.id = id;
									js.src = "https://platform.twitter.com/widgets.js";
									fjs.parentNode.insertBefore(js, fjs);
								}
							}(document, "script", "twitter-wjs");
						</script>
					</div>
				</div>
				<p>
					Premios
				</p>
				<div  style="width:100%; min-height:600px; background-color:white; ">

					<?php
					$query = $this -> db -> get('news');

					foreach ($query->result_array() as $row) {
						echo '<div class="item n_' . $row['id'] . '" onclick="set_mod($(this),{img:\'' . base_url('imagenes/' . $row['image_url']) . '\',title:\'' . str_replace("'", "\'", str_replace('%', '&#37;', htmlentities($row['title'], ENT_QUOTES | ENT_DISALLOWED))) . '\',content:\'' . str_replace("'", "\'", str_replace('%', '&#37;', $row['description'])) . '\',url:\'' . str_replace("'", "\'", $row['link_url']) . '\'})">' . "\n" . '<div class="item_img" style="background-image:url(' . base_url('imagenes/' . $row['image_url']) . ');" >' . "\n" . '</div>' . str_replace('%', '&#37;', htmlentities(substr($row['title'], 0, 20), ENT_QUOTES | ENT_DISALLOWED)) . '...' . "\n" . '<div class="item_more">Ver más</div></div>';
					}
					?>

					<div style="clear:both;"></div>

					</div>

					<p>
						Beneficios
					</p>
					<div style="width:100%; min-height:600px;">

						<?php
						$query = $this -> db -> get('news');

						foreach ($query->result_array() as $row) {
							echo '<div class="item n_' . $row['id'] . '" onclick="set_mod($(this),{img:\'' . base_url('imagenes/' . $row['image_url']) . '\',title:\'' . str_replace("'", "\'", str_replace('%', '&#37;', htmlentities($row['title'], ENT_QUOTES | ENT_DISALLOWED))) . '\',content:\'' . str_replace("'", "\'", str_replace('%', '&#37;', $row['description'])) . '\',url:\'' . str_replace("'", "\'", $row['link_url']) . '\'})">' . "\n" . '<div class="item_img" style="background-image:url(' . base_url('imagenes/' . $row['image_url']) . ');" >' . "\n" . '</div>' . str_replace('%', '&#37;', htmlentities(substr($row['title'], 0, 20), ENT_QUOTES | ENT_DISALLOWED)) . '...' . "\n" . '<div class="item_more">Ver más</div></div>';
						}
						?>

						<div style="clear:both;"></div>

						</div>

					</div>
				</div>
				<div style="clear:both;"></div>
				<script>
					$(document).ready(function()
{<?php

if(isset($id) && $id != null)
{
?>
$('.<?= $id ?>').trigger('click');<?php
}
else
{
?>
	var random = parseInt(Math.random() * (($('.item').length - 1) - 0) + 0);
	$('.item').eq(random).trigger('click'); 
<?php
}
?>
	});

				</script>

				<?php
				$this -> load -> view('copyright');
				?>

				<!--Fin de contenido -->
			</div>

		</div>
