<div class="footer">

	<div class="txtFooter" <?= $this -> router -> class == 'landing' ? 'style="color:white; text-shadow: #000 2px 2px 3px;"' : '' ?>>
		La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco.
		<a href="<?= base_url('legal/terminos_y_condiciones') ?>"  target="_blank"  <?= $this -> router -> class == 'landing' ? 'style="color:white;"' : '' ?> ><b>Términos y condiciones de uso del Programa ADDICTED</b></a> | 
		<a href="<?= base_url('legal/aviso_de_privacidad') ?>"  target="_blank"  <?= $this -> router -> class == 'landing' ? 'style="color:white;"' : '' ?> ><b>Aviso de Privacidad</b></a>
	</div>

	<div class="logoAntara"><img src="imagenes/antara-logo.png "/>
	</div>

</div>