<?php

$controller = $this -> router -> class;
?>

<!DOCTYPE html>

<html  lang="es">

	<head>
		<meta charset="utf-8" />
		<title>Addicted</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Addicted es el nuevo plan de Antara Fashion Hall que te da  beneficios y grandes premios. Vivir el estilo addicted es sencillo." />
		<meta name="keywords" content="beneficios, premios, lealtad, addicted, antara, entradas, eventos, estacionamiento, descuentos, VIP, certificados, lujo, concierge, servicio, experiencias, black, platinum, clear, puntos, premiere" />
		<link rel="icon" type="image/png" href="<?= base_url('imagenes/favicon.png') ?>" />
		<link rel="shortcut icon" sizes="196x196" href="<?php echo base_url().'imagenes/icono.png' ?>">
		<link rel="apple-touch-icon-precomposed" href="<?php echo base_url().'imagenes/icono.png' ?>" />
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<!-- Carga de estilos generales -->
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/panel.css') ?>" />
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/main.css') ?>" />
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/views/novedades.css') ?>" />
		<link rel="stylesheet" type="text/css" media="screen" href="<?= base_url('theme/supersized.shutter.css') ?>" />
			 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />

		<!-- Carga de Scripts -->
		<script type="text/javascript" src="<?= base_url('js/modernizr.js') ?>"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<script type="text/javascript" src="<?= base_url('js/autocomplete.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/funcionesGenerales.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/validaLogin.js') ?>"></script>
			<script type="text/javascript" src="<?= base_url('js/validaPreregistro.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/jquery.cycle.all.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/slideTop.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/views/novedades.js') ?>"></script>


		<!--Script y CSS de Premios y Beneficios 
		<?php
		if (file_exists('css/views/' . $controller . '.css')) {echo '<link rel="stylesheet" type="text/css" href="' . base_url('css/views/' . $controller . '.css') . '?' . date('Y-m-d_h:i:s') . '" />';
		}
		?>
		<?php
		if (file_exists('js/views/' . $controller . '.js')) {echo '<script type="text/javascript" src="' . base_url('js/views/' . $controller . '.js') . '?' . date('Y-m-d_h:i:s') . '"></script>';
		}
		?>
-->
		<?php
/* Slide de imagenes del fondo |Landing */
if ($controller == "landing") {
		?>

		<!--Scripts para slide de imagenes -->
		<script type="text/javascript" src="<?= base_url('js/jquery.easing.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('js/supersized.3.2.7.min.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('theme/supersized.shutter.min.js') ?>"></script>
		<!-- Carga de estilos y script de slide para el fondo -->
		<link rel="stylesheet" href="<?= base_url('css/supersized.css') ?>" type="text/css" media="screen" />
		<script type="text/javascript" src="<?= base_url('js/slideImagenesLanding.js') ?>"></script>
		
		<meta name="Con Addicted, el plan de lealtad de Antara Fashion Hall, tu compras se convierten en viajes a presenciar los eventos de moda más importantes, presencias los mejores eventos deportivos, asistencia a los mejores conciertos, viajes a los lugares más exclusivos del mundo"/>

 

<meta name="keywords" content="addicted, plan de lealtad, premios, beneficios, descuentos, antara">

                        <meta name="robots" content="index, follow">

                        <meta name="revisit-after" content="4 days">

                        <meta name="alexa" content="100">

                        <meta name="pagerank™" content="10">

                        <meta name="distribution" content="global">
		

		<?php
		}
		?>

		<?php
		/* Condicion para Descubre */
		if($controller == "descubre") {
		?>
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/descubre.css') ?>" />
		
<meta name="Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios como: Viajes para presenciar las entregas de los premios más afamados, como los Oscar y Grammy´s, boletos a los mejores conciertos alrededor del mundo, con pases VIP y al backstage, entradas a los eventos deportivos más importantes en el mundo como el Super Bowl, US Open, Grand Prix y Wimbledon, viajes a los destinos más exclusivos y acceso a los eventos de moda de mayor prestigio, como al Fashionweek en Paris o Nueva York. Addicted ofrece los mejores beneficios como: Estacionamiento gratis dentro de Antara Fashion Hall, descuentos y promociones especiales e invitaciones a los eventos VIP en Antara Fashion Hall"/>

 

<meta name="keywords" content="addicted, plan de lealtad, premios, beneficios, descuentos, VIP">

                        <meta name="robots" content="index, follow">

                        <meta name="revisit-after" content="4 days">

                        <meta name="alexa" content="100">

                        <meta name="pagerank™" content="10">

                        <meta name="distribution" content="global">
		<?php
		}
		?>



		<!-- Condicion para  Contacto -->
		<?php
		if($controller == "contacto") {
		?>
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/contacto.css') ?>" />
		
		<meta name="description" content="Nuestro Concierge está ubicado en Planta Baja"/>

 

<meta name="keywords" content="concierge antara">

                        <meta name="robots" content="index, follow">

                        <meta name="revisit-after" content="4 days">

                        <meta name="alexa" content="100">

                        <meta name="pagerank™" content="10">

                        <meta name="distribution" content="global">
		<?php
		}
		?>
		
		<?php
			if($this->session->flashdata('notification') != false)
			{
				echo '<script type="text/javascript">alert(\''.$this->session->flashdata('notification').'\');</script>';
			}
		?>
		
		
	<script>
	
		function setNotification(str)
		{
			alert(str);
		}
	
		<?php
			
			$errors = array(1 => 'El e-mail o contraseña proporcionados son incorrectos.',
							2 => 'El e-mail y contraseña son inválidos.',
							3 => 'Error en e-mail ingresado, revise su información e intente nuevamente.',
							4 => 'El acceso de recuperación no es válido o ha expirado. Favor de acudir con su concierge Antara.',
							5 => 'Su contraseña ha sido ajustada, ahora puede conectarse.',
							6 => 'Le ha sido enviado un e-mail a su dirección registrada, por favor revise y continúe con las instrucciones de su e-mail.',
							7 => 'La liga de acceso es inválida, Favor de acudir con su concierge en Antara.');
			
			if(isset($_GET['e']) && array_key_exists($_GET['e'],$errors))
			{
		
		?>
				$(document).ready(function()
				{
					setNotification('<?= $errors[$_GET['e']] ?>');
				});
		<?php
		
			}
		
		?>
	</script>
	
	</head>

	<body>


		<!--Panel de menus izquierdo -->
		<div class="panel">

			<div>
				<a href="<?= base_url() ?>"><img src="imagenes/logo_addicted.png" /></a>
			</div>
			<div style="text-align: center;"><img src="imagenes/plandelealtad.png" />
			</div>

			<!--Menu de opciones-->
			<div id="menu">
				<center>
					<nav>
						
							<li>
								<a href="<?= base_url('descubre') ?>" class="boton btnmenu" <?= $this -> router -> class == 'descubre' ? 'style=" background-color: #5bb206;"' : '' ?>>Descubre addicted</a>
							</li>
							<li>
								<a href="#"class="boton btnmenu" <?= $this -> router -> class == 'experiencias' ? 'style=" background-color: #5bb206;"' : '' ?><?= $this -> router -> class == 'beneficios' ? 'style=" background-color: #5bb206;"' : '' ?><?= $this -> router -> class == 'descuentos' ? 'style=" background-color: #5bb206;"' : '' ?><?= $this -> router -> class == 'obsequios' ? 'style=" background-color: #5bb206;"' : '' ?>>Premios y beneficios</a>

								<ul>
									<li>
										<a href="<?= base_url('descuentos') ?>" class="boton btnmenu" <?= $this -> router -> class == 'descuentos' ? 'style=" background-color: #5bb206;"' : '' ?>>Descuentos</a>
									</li>
									<li>
										<a href="<?= base_url('obsequios') ?>" class="boton btnmenu" <?= $this -> router -> class == 'obsequios' ? 'style=" background-color: #5bb206;"' : '' ?>>Certificados de Regalo</a>
									</li>
									<li>
										<a href="<?= base_url('beneficios') ?>" class="boton btnmenu" <?= $this -> router -> class == 'beneficios' ? 'style=" background-color: #5bb206;"' : '' ?>>Beneficios</a>
									</li>
									<li>
										<a href="<?= base_url('experiencias') ?>" class="boton btnmenu" <?= $this -> router -> class == 'experiencias' ? 'style=" background-color: #5bb206;"' : '' ?>>Experiencias</a>
									</li>
								
								</ul>

							</li>
							<li>
								<a onclick="muestra_oculta('registro');" class="boton btnmenu">Preregístrate</a>
							</li>
							<li>
								<a href="<?= base_url('contacto') ?>" class="boton btnmenu" <?= $this -> router -> class == 'contacto' ? 'style=" background-color: #5bb206;"' : '' ?>>Contacto</a>
							</li>
						
					</nav>
				</center>
			</div>

			<div class="registro">
				<p style="font-size:1em;">
					Miembros:
				</p>
				<p style="font-size:11px;padding-top:10px;">
					Todas las contraseñas han sido restablecidas, da click <a style="color:#7ba955;" href="http://addicted.com.mx/socio/acceso">aquí</a> para crear una nueva contraseña. Continúa si es que ya tienes tu contraseña nueva.
				</p>
				<br>
				<!-- <form action="http://antara.kinetiq.mx/addicted" id="formLogin" method="post" accept-charset="utf-8" onsubmit="return false;"> -->
				<form action="http://concierge.addicted.com.mx/addicted" id="formLogin" method="post" accept-charset="utf-8">
				 <fieldset>
					<label for="email" style="font-size:0.8em;">e-mail:</label>
					
					<input   tabindex="1" type="email"  id="mail" name="mail" style="width:90%"    autocomplete="off" />
					
					<label for="password" style="font-size:0.8em;"><br>Contraseña:	</label>					
					<input  tabindex="2" type="password" name="password" id="password" style="width:90%"  autocomplete="off" />
					<br>
					<br>
					<input   type="submit" name="enviar" id="enviar" value="Entrar" style="width:50%;" class="boton"  />
					<br>
					<br>
					 </fieldset>
				</form>
				<form action="http://concierge.addicted.com.mx/addicted" id="recuperar" method="post" accept-charset="utf-8">
				<fieldset>
				<input type="hidden"   id="mailRecuperar" name="mailRecuperar"   autocomplete="off" />
				<input type="submit" name="olvidaste" id="olvidaste" value="Recuperar contraseña"  class="rPass"   />
				</fielsiet>
				</form>
			</div>
						
			<!--Telefono de contacto-->
			<div style="text-align: center; margin-top:30px; font-family:Tahoma;">
				<div style="font-size:1.5em; "><img src="imagenes/telefono.png" width="22" height="22" />4593-8870
				</div>
				<div id="web">
					<a href="http://www.antara.com.mx" target="_blank" >www.antara.com.mx</a>
				</div>
			</div>

			<!--Fin del panel-->
		</div>

