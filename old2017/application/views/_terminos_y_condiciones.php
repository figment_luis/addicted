<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<title>Addicted</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Addicted es el nuevo plan de Antara Fashion Hall que te da  beneficios y grandes premios. Vivir el estilo addicted es sencillo." />
<meta name="Con Addicted, el plan de lealtad de Antara Fashion Hall, tu compras se convierten en viajes a presenciar los eventos de moda más importantes, presencias los mejores eventos deportivos, asistencia a los mejores conciertos, viajes a los lugares más exclusivos del mundo"/>
<meta name="keywords" content="addicted, plan de lealtad, premios, beneficios, descuentos, antara">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="4 days">
<meta name="alexa" content="100">
<meta name="pagerank™" content="10">
<meta name="distribution" content="global">
<link rel="stylesheet" type="text/css" href="http://www.addicted.com.mx/css/main.css" />
<style>
    h1, h2 {
        font-family: Chalet-ParisNineteenEighty;
        margin: 24px 0;
        font-weight: 600;
        color: #005702;
    }
    
    h1 {
        font-size: 20px;
    }
    
    h2 {
        font-size: 16px;
    }
    
    p {
        font-size: 12px;
        margin-bottom: 16px;
        color: #333;
    }
    
    small {
        font-size: 10px;
        color: #666;
    }
      
    .container {
        padding: 60px 0;
        width: 790px;
        margin: 0 auto;
    }
    
    .addicted {
        
    }
   
</style>

</head>
<body>
<div class="container">
<h1>T&eacute;rminos y condiciones del programa de lealtad de Antara Fashion Hall denominado el &quot;Programa <strong class="addicted">ADDICTED</strong>&quot;.</h1>
<hr>
<h2>1. Disposiciones generales</h2>
<p>1.1 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho de modificar en cualquier momento los t&eacute;rminos y condiciones del Programa <strong class="addicted">ADDICTED</strong>. Esto implica que el Programa <strong class="addicted">ADDICTED</strong> podr&aacute; modificar el monto de los gastos requeridos para redimir una recompensa, modificar la cantidad de puntos acumulados en un tiempo determinado para pertenecer a alguno de los niveles de programa , imponer l&iacute;mites para la acumulaci&oacute;n, obtenci&oacute;n, intercambio y/o redenci&oacute;n de puntos, cancelar recompensas, previo aviso a los socios a trav&eacute;s de cualquiera de los siguientes medios de comunicaci&oacute;n que en forma enunciativa m&aacute;s no limitativa se se&ntilde;alan a continuaci&oacute;n: estado de cuenta, boletines informativos, env&iacute;os de correo directo, gu&iacute;a de recompensas, correo electr&oacute;nico, p&aacute;gina de Internet, etc.</p>
<p>1.2 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho absoluto de terminar el programa en cualquier momento, previo aviso con 30 (treinta) d&iacute;as naturales de anticipaci&oacute;n a los socios. Esto significa que el derecho de los socios para redimir los puntos acumulados en el programa se terminar una vez transcurridos 30 (treinta) d&iacute;as naturales siguientes a que el Programa <strong class="addicted">ADDICTED</strong> haya dado el aviso de terminaci&oacute;n correspondiente a los socios.</p>
<p>1.3 El socio reconoce y acepta que el Programa <strong class="addicted">ADDICTED</strong> en ning&uacute;n momento garantiza ni es responsable del cumplimiento de las obligaciones contra&iacute;das por los establecimientos participantes que ofrezcan un beneficio, descuento o promoci&oacute;n para socios <strong class="addicted">ADDICTED</strong>, as&iacute; como ning&uacute;n otro programa. No obstante lo anterior, el Programa <strong class="addicted">ADDICTED</strong> garantiza la existencia y validez de los puntos. Asimismo, el Programa <strong class="addicted">ADDICTED</strong> se compromete a ofrecer a los socios, por lo menos durante un plazo de 30 d&iacute;as contado a partir de la fecha de terminaci&oacute;n del programa, opciones para el uso de puntos.</p>
<p>1.4 Cualquier queja o reclamaci&oacute;n derivada de la elegibilidad para participar en el programa, de la acreditaci&oacute;n de puntos y/o redenci&oacute;n o intercambio de recompensas ser&aacute; resuelto por el Programa <strong class="addicted">ADDICTED</strong> a su exclusiva discreci&oacute;n. El cumplimiento de cualquier obligaci&oacute;n fiscal, ya sea de car&aacute;cter federal, local o municipal, que se derive de la acreditaci&oacute;n de puntos o redenci&oacute;n en el programa, ser&aacute; responsabilidad exclusiva del socio.</p>
<p>1.5 El programa ADDICTED está destinado a clientes particulares del centro comercial Antara Fashion Hall, por lo que no es posible registrar compras empresariales en cuentas de ningún socio particular, ni registrar compras en cuentas de empleados de locales ubicados en el mismo centro comercial.</p>
<p>1.6 Los términos y condiciones establecidos en este documento dejan sin efecto cualesquiera otros términos y condiciones relativos al programa.</p>
<h2>2. Inscripci&oacute;n</h2>
<p>2.1 La inscripci&oacute;n al Programa <strong class="addicted">ADDICTED</strong> es totalmente gratuita.</p>
<p>2.2 La membres&iacute;a y la tarjeta es personal e intransferible, por lo que los beneficios son exclusivamente para el titular de la cuenta.</p>
<p>2.3 La tarjeta pl&aacute;stica que acredita a una persona como socio del programa es propiedad del Programa <strong class="addicted">ADDICTED</strong> por lo que puede solicitar la devoluci&oacute;n de la misma en cualquier momento.</p>
<p>2.4 El programa tiene 3 niveles: <strong>Clear</strong>, socios que acrediten desde 10 y hasta 999 puntos en un periodo de 18 meses; <strong>Platinum</strong>, socios que acrediten desde 1,000 y hasta 6,999 puntos en un periodo de 18 meses y <strong>Black</strong>, socios que acrediten m&aacute;s de 7,000 puntos en un periodo de 18 meses.</p>
<p>2.5 Para inscribirse al programa, el cliente deberá de realizar compra(s) mayor(es) o igual(es) a $500.00 (quinientos pesos) en cualquiera de los establecimientos participantes. Los tickets de compra que se obtengan de diferentes tiendas y que sean de la misma fecha podrán ser acumulables presentándolos en el área de concierge. Son considerados tickets de compra válidos para registro de puntos addicted, todos los tickets de compra pagados con una o varias de las siguientes formas de pago: tarjetas de crédito, tarjetas de debito y efectivo.</p>
<p>2.6 El cliente deber&aacute; de llenar un formato de registro, con datos de su perfil, en el &aacute;rea de concierge para finalizar el proceso de inscripci&oacute;n.</p>
<p>2.7 Al inscribirse en el Programa <strong class="addicted">ADDICTED</strong>, el socio declara ser mayor de 18 a&ntilde;os y al utilizar los beneficios del programa, da su consentimiento en someterse a las Reglas del Programa <strong class="addicted">ADDICTED</strong>.</p>
<p>2.8 La inscripci&oacute;n al programa surte sus efectos a partir de la fecha de activaci&oacute;n de la tarjeta de membres&iacute;a correspondiente.</p>
<h2>3. Estado de cuenta</h2>
<p>3.1 Los socios podr&aacute;n verificar su estado de cuenta en cualquier momento en <a href="http://www.addicted.com.mx">www.<strong class="addicted">ADDICTED</strong>.com.mx</a> entendiendo que la informaci&oacute;n puede variar seg&uacute;n los movimientos que se hayan realizado y que el sistema no haya registrado al momento de la consulta.</p>
<p>3.2 El socio podr&aacute; consultar su estado de cuenta en el &aacute;rea de concierge.</p>
<h2>4. Acumulaci&oacute;n de puntos</h2>
<p>4.1 &Uacute;nicamente podr&aacute;n ser acumulados los puntos que se generen a partir de la fecha de inscripci&oacute;n del socio.</p>
<p>4.2 Los socios <strong>Clear</strong> tendr&aacute;n derecho a la acumulaci&oacute;n de un (1) punto por cada $50.00 (cincuenta pesos) gastados.</p>
<p>4.3 Los socios <strong>Platinum</strong> tendr&aacute;n derecho a la acumulaci&oacute;n de tres (3) puntos por cada $50.00 (cincuenta pesos) gastados.</p>
<p>4.4 Los socios <strong>Black</strong> tendr&aacute;n derecho a la acumulaci&oacute;n de cuatro (4) puntos por cada $50.00 (cincuenta pesos) gastados.</p>
<p>4.5 La acumulaci&oacute;n de puntos s&oacute;lo podr&aacute; realizarse en el &aacute;rea de concierge presentando el ticket de compra y la tarjeta <strong class="addicted">ADDICTED</strong>.</p>
<p>4.6 Los tickets de compra tendr&aacute;n una vigencia de 15 d&iacute;as despu&eacute;s de la compra para acreditar puntos.</p>
<p>4.7 Existen dos tipos de puntos; Puntos en Transici&oacute;n, son aquellos puntos que durante los primeros 30 d&iacute;as despu&eacute;s de su compra no se podr&aacute;n utilizar. Puntos Firmes, puntos que podr&aacute; utilizar para la redenci&oacute;n de premios o experiencias <strong class="addicted">ADDICTED</strong>.</p>
<p>4.8 Son considerados tickets de compra válidos para registro de puntos addicted, todos los tickets de compra expedidos por cualquier locatario ubicado en Antara Fashion Hall pagados con una o varias de las siguientes formas de pago: tarjetas de crédito, tarjetas de debito y efectivo; quedando excluidos de participar en el programa ADDICTED operaciones realizadas en PlayCity Casino, bancos, ticket master y superboleto.</p>
<h2>5. Redenci&oacute;n</h2>
<p>5.1 Para poder redimir un premio el socio deber&aacute; acudir al &aacute;rea de Concierge donde seleccionar&aacute; el premio o experiencia de su preferencia del cat&aacute;logo de premios y experiencias vigentes al momento de su visita.</p>
<p>5.2 El concierge validar&aacute; en el sistema si sus puntos son suficientes para la redenci&oacute;n y continuar&aacute; el proceso con la autorizaci&oacute;n del sistema llenando el formato de redenciones.</p>
<p>5.3 La entrega f&iacute;sica se har&aacute; en el &aacute;rea de Concierge del Programa <strong class="addicted">ADDICTED</strong>.</p>
<p>5.4 Los premios y experiencias &uacute;nicamente se entregar&aacute;n al titular de la membres&iacute;a presentando una identificaci&oacute;n oficial vigente y su tarjeta addicted.</p>
<p>5.5 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho de modificar las fechas que definen la redenci&oacute;n de las experiencias y premios.</p>
<p>5.6 La calidad de los premios y experiencias es responsabilidad del proveedor que las ofrece.</p>
<p>5.7 Los premios son parte de un convenio con una(s) empresa(s) externa(s) a <strong class="addicted">ADDICTED</strong>. En caso de cualquier aclaraci&oacute;n o reclamaci&oacute;n de los productos la deber&aacute; realizar al Concierge <strong class="addicted">ADDICTED</strong> a m&aacute;s tardar 30 d&iacute;as despu&eacute;s de la fecha de entrega, posterior a esta fecha deber&aacute; de contactar directamente al proveedor correspondiente.</p>
<p>5.8 En ning&uacute;n caso los puntos aplican para art&iacute;culos de las tiendas participantes ni por dinero en efectivo.</p>
<p>5.9 Cada experiencia tiene requisitos que el socio debe cumplir, es responsabilidad del socio asegurarse de cumplirlos ya que no habr&aacute; reembolso una vez que se compre la experiencia.</p>
<h2>6. Vigencia</h2>
<p>6.1 Para niveles <strong>Clear</strong>, <strong>Platinum</strong> y <strong>Black</strong> los puntos tendr&aacute;n una vigencia de vencimiento de 18 meses despu&eacute;s de la acreditaci&oacute;n de puntos.</p>
<p>6.2 Los socios tendr&aacute;n 30 d&iacute;as para recoger los premios o experiencias solicitados en Concierge.</p>
<h2>7. Cancelaci&oacute;n</h2>
<p>7.1 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho de suspender temporalmente o dar de baja de manera definitiva a cualquier socio del programa por incumplimiento de sus condiciones generales o por as&iacute; convenir a los intereses del programa. Bastar&aacute;n tan s&oacute;lo 24 horas naturales de notificaci&oacute;n escrita para avisar al socio v&iacute;a correo electr&oacute;nico.</p>
<p>7.2 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho de cancelar la membres&iacute;a a cualquier socio que a juicio del programa <strong class="addicted">ADDICTED</strong> haya realizado o sea sospechoso (a entero criterio del Programa <strong class="addicted">ADDICTED</strong>) de fraude, abuso o violaci&oacute;n a alguno de los cr&eacute;ditos de puntos, beneficios, uso de recompensas o cualquier otra reglamentaci&oacute;n del Programa <strong class="addicted">ADDICTED</strong>.</p>
<p>7.3 La suspensi&oacute;n definitiva tiene como consecuencia la cancelaci&oacute;n total de los puntos. A partir de ese momento, no se podr&aacute;n acumular, redimir o canjear puntos, ni podr&aacute; volverse a inscribir al programa.</p>
<p>7.4 Si el socio <strong>Clear</strong>, <strong>Platinum</strong> y <strong>Black</strong> no genera ning&uacute;n movimiento durante 18 meses, su participaci&oacute;n se dar&aacute; por cancelada con previo aviso.</p>
<p>7.5 En caso de que el socio requiera cancelar su inscripci&oacute;n al Programa <strong class="addicted">ADDICTED</strong>, deber&aacute; solicitarlo en el &aacute;rea de concierge o v&iacute;a correo electr&oacute;nico a <a href="mailto:concierge@antara.com.mx">concierge@antara.com.mx</a> o por escrito en las oficinas del Programa <strong class="addicted">ADDICTED</strong>, ubicadas en: Ej&eacute;rcito Nacional no.843 B, S&oacute;tano 2, entrada Cervantes Saavedra, Col. Granada, Del. Miguel Hidalgo, CP. 11520, M&eacute;xico, D.F., en cuyo caso, el socio se someter&aacute; a los t&eacute;rminos y condiciones de esta secci&oacute;n.</p>
<p>7.6 El Programa <strong class="addicted">ADDICTED</strong>, podr&aacute; suspender en cualquier momento los beneficios de esta tarjeta por as&iacute; convenir a sus intereses o por mal uso de la misma, en virtud que los beneficios son intransferibles.</p>
<h2>8. Consulta o atenci&oacute;n al socio</h2>
<p>8.1 Es deber de todos los socios conocer y cumplir con las condiciones generales del Programa <strong class="addicted">ADDICTED</strong>, as&iacute; como mantenerse actualizado de los posibles cambios de su perfil a trav&eacute;s de la p&aacute;gina de Internet o Concierge.</p>
<p>8.2 El Programa Addicted se reserva el derecho de modificar, suspender temporal o definitivamente, una o todas las condiciones del programa sin previo aviso y sin ninguna responsabilidad.</p>
<p>8.3 El Programa <strong class="addicted">ADDICTED</strong> mantendr&aacute; una comunicaci&oacute;n constante con sus socios a trav&eacute;s del correo electr&oacute;nico que nos haya proporcionado.</p>
<p>8.4 Si su tarjeta ha sido robada o extraviada es obligaci&oacute;n del socio notificarlo oportunamente ya que <strong class="addicted">ADDICTED</strong> no se hace responsable por el mal uso de sus puntos.</p>
<p>8.5 <strong class="addicted">ADDICTED</strong> se reserva el derecho de cambiar el valor en puntos de los premios y experiencias sin previo aviso.</p>
<h2>9. Privacidad</h2>
<p>9.1 El Programa <strong class="addicted">ADDICTED</strong> se reserva el derecho de publicar los nombres, informaci&oacute;n general y personal e im&aacute;genes de los socios participantes en promociones del programa.</p>
<p>9.2 Los datos proporcionados por los socios durante la inscripci&oacute;n son resguardados por el Programa <strong class="addicted">ADDICTED</strong> siguiendo los t&eacute;rminos definidos en nuestro&nbsp;Aviso de Privacidad.</p>
<p>9.3 Datos estad&iacute;sticos pueden ser utilizados para mejorar el programa y s&oacute;lo se podr&aacute;n utilizar para el Programa <strong class="addicted">ADDICTED</strong>.</p>
<p>9.4 La administraci&oacute;n de addicted realizar&aacute; auditorias constantes para asegurar el buen uso de las tarjetas, puntos y redenci&oacute;n de premios o experiencias.</p>
<hr>
<small>** Estos T&eacute;rminos y Condiciones entrar&aacute;n en vigencia a partir del 10 de julio de 2013.</small>
<br><br>
</div>
</body>
</html>