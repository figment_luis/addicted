<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<title>Addicted</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Addicted es el nuevo plan de Antara Fashion Hall que te da  beneficios y grandes premios. Vivir el estilo addicted es sencillo." />
<meta name="Con Addicted, el plan de lealtad de Antara Fashion Hall, tu compras se convierten en viajes a presenciar los eventos de moda más importantes, presencias los mejores eventos deportivos, asistencia a los mejores conciertos, viajes a los lugares más exclusivos del mundo"/>
<meta name="keywords" content="addicted, plan de lealtad, premios, beneficios, descuentos, antara">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="4 days">
<meta name="alexa" content="100">
<meta name="pagerank™" content="10">
<meta name="distribution" content="global">
<link rel="stylesheet" type="text/css" href="http://www.addicted.com.mx/css/main.css" />
<style>
    h1, h2, h3 {
        font-family: Chalet-ParisNineteenEighty;
        margin: 24px 0;
        font-weight: 600;
        color: #005702;
    }
    
    h1 {
        font-size: 20px;
    }
    
    h2 {
        font-size: 16px;
    }
    
    h3 {
        font-size: 14px;
    }
    
    p {
        font-size: 12px;
        margin-bottom: 16px;
        color: #333;
    }
    
    small {
        font-size: 10px;
        color: #666;
    }
      
    .container {
        padding: 60px 0;
        width: 790px;
        margin: 0 auto;
    }
    
    .addicted {
        
    }
   
</style>

</head>
<body>
<div class="container">
<h1>Aviso de Privacidad</h1>
<hr>
<p><strong>Grupo Integral de Desarrollo Inmobiliario, S. de R.L. de C.V. (Antara)</strong> (en lo sucesivo, el <i>Responsable</i>), con domicilio en Avenida Ej&eacute;rcito Nacional 843-B, Colonia Granada, Ciudad de M&eacute;xico, Delegaci&oacute;n Miguel Hidalgo, C.P. 11520, es responsable del tratamiento de sus datos personales.</p>
<p>En estricto cumplimiento a lo establecido en la Ley Federal de Protecci&oacute;n de Datos Personales en Posesi&oacute;n de los Particulares (la Ley), el Responsable hace de su conocimiento lo siguiente:</p>
<h2>1. Datos de contacto del Responsable</h2>
<p><strong>Oficina de Privacidad:</strong> M&oacute;dulo de Conserjer&iacute;a de Antara (<strong class="addicted">Concierge</strong>)<br>
<strong>Domicilio:</strong> Avenida Ej&eacute;rcito Nacional 843-B, Colonia Granada, Ciudad de M&eacute;xico, Delegaci&oacute;n Miguel Hidalgo, C.P. 11520<br>
<strong>Correo electr&oacute;nico:</strong> <a href="mailto:avisoprivacidad@antara.com.mx">avisoprivacidad@antara.com.mx</a><br>
<strong>Tel&eacute;fono:</strong> 4593 8870</p>
<h2>2. &iquest;Qu&eacute; informaci&oacute;n o datos recopilamos?</h2>
<p>Los Datos Personales que Usted, de manera libre y voluntaria, proporcione al Responsable a trav&eacute;s de los sitios web <a href="www.antara.com.mx">www.antara.com.mx</a> y <a href="www.antara.com.mx">www.addicted.com.mx</a>, mediante los servicios de <strong class="addicted">Concierge</strong> y/o por v&iacute;a telef&oacute;nica a trav&eacute;s del Centro de Atenci&oacute;n a Clientes, podr&aacute;n incluir, entre otros, los siguientes: nombre, domicilio, fecha de nacimiento, g&eacute;nero, direcci&oacute;n de correo electr&oacute;nico, n&uacute;meros telef&oacute;nicos, estado civil y n&uacute;meros de membres&iacute;a de los programas operados por terceros asociados con nuestro programa de lealtad.</p>
<h2>3. &iquest;Qu&eacute; uso se le da a esta informaci&oacute;n?</h2>
<p>Los Datos Personales recopilados, se utilizan &uacute;nicamente para los siguientes fines: <strong>(i)</strong> identificaci&oacute;n y verificaci&oacute;n del titular, <strong>(ii)</strong> contacto, <strong>(iii)</strong> para informar al titular de su estatus en el programa de lealtad <strong class="addicted">ADDICTED</strong>, <strong>(iv)</strong> registro para ser destinatario de nuestros boletines electr&oacute;nicos, <strong>(v)</strong> env&iacute;o de promociones de nuestros servicios, de los productos o servicios de nuestros locatarios y empresas aliadas, <strong>(vi)</strong> fines estad&iacute;sticos y de an&aacute;lisis internos y, <strong>(vii)</strong> ofrecimiento para la participaci&oacute;n en eventos y promociones organizados por el Responsable.</p>
<h2>4. &iquest;Con qui&eacute;n compartimos su informaci&oacute;n y bajo qu&eacute; t&eacute;rminos?</h2>
<p>Sus Datos Personales pueden ser transferidos y tratados, sin fines comerciales, dentro y fuera del pa&iacute;s, por personas distintas al Responsable. En ese sentido, su informaci&oacute;n puede ser compartida con terceros que presten servicios relacionados con el manejo y administraci&oacute;n de datos.</p>
<p>En ning&uacute;n caso comercializaremos, venderemos o rentaremos su informaci&oacute;n personal a un tercero sin contar con su consentimiento previo.</p>
<p>Si usted no manifiesta su oposici&oacute;n para que sus Datos Personales sean transferidos, se entender&aacute; que ha otorgado su consentimiento para ello.</p>
<h2>5. &iquest;Qu&eacute; medidas de seguridad y control utilizamos para la protecci&oacute;n de sus datos personales?</h2>
<p>El Responsable tiene implementadas medidas de seguridad administrativas, t&eacute;cnicas y f&iacute;sicas para proteger sus Datos Personales, mismas que igualmente exigimos sean cumplidas por los proveedores de servicios que contratamos, inclusive trat&aacute;ndose de servicios que prestan las empresas subsidiarias o afiliadas del Responsable. Toda la informaci&oacute;n agregada y datos personales que se obtengan de usted a trav&eacute;s del uso de nuestros sitios <a href="www.antara.com.mx">www.antara.com.mx</a> y <a href="www.antara.com.mx">www.addicted.com.mx</a>, constituir una base de datos propiedad del Responsable, informaci&oacute;n que es almacenada con el fin de protegerla y evitar su p&eacute;rdida, uso indebido, o alteraci&oacute;n.</p>
<h2>6. &iquest;Cu&aacute;l es el &aacute;rea responsable del manejo y administraci&oacute;n de Datos Personales?</h2>
<p>El &aacute;rea responsable del manejo y administraci&oacute;n de sus Datos Personales es el M&oacute;dulo de Conserjer&iacute;a de Antara, mismo que puede ser contactada mediante correo electr&oacute;nico, a la direcci&oacute;n <a href="mailto:avisoprivacidad@antara.com.mx">avisoprivacidad@antara.com.mx</a>, o f&iacute;sicamente en las instalaciones del Responsable, ubicadas en Avenida Ej&eacute;rcito Nacional 843-B, Colonia Granada, Ciudad de M&eacute;xico, Delegaci&oacute;n Miguel Hidalgo, C.P. 11520.</p>
<h2>7. &iquest;C&oacute;mo puede Usted ejercer sus derechos de Acceso, Rectificaci&oacute;n, Cancelaci&oacute;n y Oposici&oacute;n (Derechos <strong class="addicted">ARCO</strong>)?</h2>
<p>De conformidad con lo establecido en el art&iacute;culo 28 de la Ley, Usted tiene el derecho de solicitar al Responsable en cualquier momento el acceso a sus Datos Personales, la rectificaci&oacute;n de los mismos en caso de ser estos inexactos o incompletos, la cancelaci&oacute;n en el uso de sus datos cuando considere que est&aacute;n siendo usados para finalidades no consentidas o la oposici&oacute;n al tratamiento de los datos para fines de brindar protecci&oacute;n a su esfera personal &iacute;ntima.</p>
<p>Los mecanismos que se han implementado para el ejercicio de sus Derechos <strong class="addicted">ARCO</strong> son a trav&eacute;s de la presentaci&oacute;n de la solicitud respectiva, poniendo a su disposici&oacute;n dos canales para realizar el tr&aacute;mite:</p>
<h3>Proceso en el M&oacute;dulo de <strong class="addicted">Concierge</strong></h3>
<p>a) Dirigirse al M&oacute;dulo de <strong class="addicted">Concierge</strong>.</p>
<p>b) Indicar al <strong class="addicted">Concierge</strong> que desea realizar alguna rectificaci&oacute;n, cancelaci&oacute;n u oposici&oacute;n a sus Datos Personales.</p>
<p>c) Acreditar su identidad mostrando su tarjeta del programa de lealtad <strong class="addicted">ADDICTED</strong> y una identificaci&oacute;n oficial vigente con fotograf&iacute;a.</p>
<p>d) Firmar el formato correspondiente, mismo que ser&aacute; llenado por el <strong class="addicted">Concierge</strong>, en donde asentar&aacute; la rectificaci&oacute;n, cancelaci&oacute;n y oposici&oacute;n a los datos que Usted solicite y al cabo de 3 (tres) d&iacute;as h&aacute;biles estos se ver&aacute;n reflejados en el sistema.</p>
<p>e) Si Usted lo desea puede recibir un email de confirmaci&oacute;n de que la modificaci&oacute;n solicitada fue llevada a cabo, para lo cual solo deber&aacute; indicar a que cuenta de correo desea recibir el aviso</p>
<h3>Proceso v&iacute;a electr&oacute;nica</h3>
<p>a) Enviar un correo electr&oacute;nico a la direcci&oacute;n de correo electr&oacute;nico  <a href="mailto:avisoprivacidad@antara.com.mx">avisoprivacidad@antara.com.mx</a> desde la cuenta de correo electr&oacute;nico con que Usted est&aacute; inscrito al programa de lealtad <strong class="addicted">ADDICTED</strong>.</p>
<p>b) Incluir en el cuerpo del correo la indicaci&oacute;n de los Datos Personales que solicita rectificar, cancelar u oponer. En el Asunto del correo deber&aacute; incluir la siguiente leyenda Solicitud <strong class="addicted">ARCO</strong> de Datos Personales.</p>
<p>c) El departamento responsable realizar&aacute; las modificaciones pertinentes en el sistema y al cabo de 3 (tres) d&iacute;as h&aacute;biles estas se ver&aacute;n reflejadas en el sistema.</p>
<p>d) Usted recibir&aacute; un mensaje de confirmaci&oacute;n de que la modificaci&oacute;n solicitada fue llevada a cabo, en la cuenta de correo electr&oacute;nico desde la que hizo la solicitud.</p>
<h2>8. Uso de cookies y web beacons</h2>
<p>Las cookies son archivos de texto que son descargados autom&aacute;ticamente y almacenados en el disco duro del equipo de c&oacute;mputo del usuario al navegar en una p&aacute;gina de Internet espec&iacute;fica, que permiten recordar al servidor de Internet algunos Datos Personales sobre este usuario, entre ellos, sus  preferencias para la visualizaci&oacute;n de las p&aacute;ginas en ese servidor, nombre y contrase&ntilde;a. Por su parte, las web beacons son im&aacute;genes insertadas en una p&aacute;gina de Internet o correo electr&oacute;nico, que pueden ser utilizadas para monitorear el comportamiento de un visitante, como almacenar informaci&oacute;n sobre la direcci&oacute;n IP del usuario, duraci&oacute;n del tiempo de interacci&oacute;n en dicha p&aacute;gina y el tipo de navegador utilizado, entre otros. Le informamos que utilizamos cookies y web beacons para obtener informaci&oacute;n personal de Usted, tal como: su tipo de navegador y sistema operativo, las p&aacute;ginas de Internet que visita, los v&iacute;nculos que sigue, la direcci&oacute;n IP, el sitio que visita antes de entrar al nuestro. El Responsable puede utilizar estas cookies y otras tecnolog&iacute;as  para confirmar su identificaci&oacute;n al tener acceso a nuestros sitios, con el solo prop&oacute;sito de otorgarle un servicio personalizado debido a que nos permite determinar sus preferencias mediante el registro de su uso en nuestros sitios por parte de los usuarios y de esta manera identificar sus h&aacute;bitos y preferencias para ofrecerle nuestros servicios, productos y/o servicios de nuestros locatarios. Mediante el uso de las &quot;cookies&quot;, no recabamos datos personales de los usuarios en t&eacute;rminos de la Ley</p>
<h2>9. Modificaciones al Aviso de Privacidad</h2>
<p>Nos reservamos el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente Aviso de Privacidad, para cumplir en todo momento con las disposiciones legales aplicables, pol&iacute;ticas internas o nuevos requerimientos, para la prestaci&oacute;n u ofrecimiento de nuestros servicios y para el ofrecimiento de productos y/o servicios de nuestros locatarios. Estas modificaciones estar&aacute;n disponibles al p&uacute;blico a trav&eacute;s de los siguientes medios: <strong>a.</strong> para consulta en el m&oacute;dulo de <strong class="addicted">Concierge</strong>, <strong>b.</strong> en nuestros sitios www.antara.com.mx y www.addicted.com.mx (secci&oacute;n aviso de privacidad) y, <strong>c.</strong> en link incluido en el pie de p&aacute;gina de nuestros Newsletters.</p>
<p>Si Usted considera que en alg&uacute;n momento su derecho de protecci&oacute;n de Datos Personales ha sido lesionado por cualquier conducta de nuestros empleados o por nuestras actuaciones o respuestas, o presume que en el tratamiento de sus Datos Personales existe alguna violaci&oacute;n a las disposiciones previstas en la Ley, podr&aacute; interponer la queja o denuncia correspondiente ante el Instituto Federal de Acceso a la Informaci&oacute;n. Para mayor informaci&oacute;n, puede dirigirse al sitio <a href="http://www.ifai.org.mx">www.ifai.org.mx</a>.</p>
<hr>
<small>** Fecha de &uacute;ltima actualizaci&oacute;n de este Aviso de Privacidad: 10/07/2013.</small>
<br><br>
</div>
</body>
</html>