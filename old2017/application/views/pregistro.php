<div class="preRegistro" id="registro">
<div class="rVerde">
	<form name="preRegistro"  id="preRegistro" method="post" action="<?= base_url('proceso') ?>"  accept-charset="utf-8">
	<input type="hidden" name="id" >
	<div class="titulo">
	<div style="position:absolute; top:-30px; right:5px;" class="boton" onclick="muestra_oculta('registro')">[cerrar]</div>
		<font size="5px">Preregistro</font> <font size="2px">introduzca los datos que acontinuación se requieren.</font>
	</div>
	<div class="lineaForm" style="margin-top:-5px;"></div>
	<div style="height:120px;">
<div style="display:block;">
	<div style="font-size:10px;padding: 0px 10px;">
		<p>Si Usted recibió la tarjeta del programa de lealtad addicted puede preregistrarla, sólo ingrese sus datos en esta sección y en su siguiente visita pase al modulo de Concierge para activarla y comenzar a disfrutar los beneficios que la tarjeta le otorga en Antara.</p>
		<br>
		<p>Si Usted no ha recibido la tarjeta addicted pero desea disfrutar los mismos beneficios, puede seguir el procedimiento anterior y luego acudir a Concierge con sus tickets de compra del día de su visita para que le entreguen una tarjeta addicted. Aplican restricciones.</p>
		<br>
	</div>
	<div class="seccion"><label for="nombre">*Nombre:</label><br><input type="text" class="caja" placeholder="Nombre"  name="nombre" id="nombre" required /></div>
	<div class="seccion"><label for="apeP">*Apellido Paterno:</label><br><input type="text"  class="caja" placeholder="Apellido Paterno" name="apeP"  id="apeP"  required /></div>
	<div class="seccion"><label for="apeM">Apellido Materno:</label><br><input type="text" class="caja" placeholder="Apellido materno" name="apeM"  id="apeM"  /></div>
</div>

<div  style="display:block; margin-top:10px;">	</div>
	
<div class="seccion" style="text-align:left;"><label for="sexo"><br>*Genero:</label><br>
<input type="radio"  name="sexo" style="width:10px; margin-right:5px;" value="F" id="F"  required /><label for="F">Femenino</label>
<input type="radio" name="sexo" style="width:10px; margin-right:5px;" value="M"  id="M" required  /><label for="M">Masculino</label>
</div>
<div class="seccion" ><label for="nacimiento"><br>*Fecha de nacimiento: </label><br><input type="text" id="datepicker" name="nacimiento" id="nacimiento"  class="caja" autocomplete="off" required /> </div>
<div class="seccion" ><input type="text" id="alternate" size="30"  class="cajatransparent"   disabled="disabled"  /></div>

</div>

<br>
	<div class="lineaForm"></div>
	
		<div>
		
			<div class="seccion"><label for="estado">*Estado:</label><br><?= setSelect('estado',estadosArray(),'Seleccione',array('id'=>'estado','label'=>'Estado','arr_assoc'=>true,'class'=>'caja')); ?></div>
			
			<div class="seccion municipio"><label for="municipio">*Municipio:</label><br><select class="caja" name="municipio" id="municipio" title="Municipio" required></select></div>
			
			<div class="seccion colonia"><label for="colonia">*Colonia:</labe><br><select class="caja" name="colonia" id="colonia" title="Colonia" required></select></div>
			
			<div style="clear:both;"></div>
			
			<div class="seccion ccp" style="opacity:0;"><label for="postal_code">*Código postal:</label><br><input class="caja" id="ccp" name="postal_code" class="input_quarter numeric" type="text" required /></div>
		
		</div>

	<div class="lineaForm"></div>
	<div style="height:80px; margin-top:-10px;">

		<div class="seccion"><label for="calle">*Calle:</label><br><input type="text" class="caja" placeholder="Calle" id="calle"   name="calle" required /></div>
	<div class="seccion">*Número exterior:<br><input type="text"  class="caja" placeholder="número exterior"  id="exterior" name="exterior" required /></div>
	<div class="seccion">Número interior:<br><input type="text" class="caja" placeholder="número interior"  id="interior" name="interior"  /></div>

	</div>
	
	<div class="lineaForm"  style="margin-top:-20px; "></div>
	<div style="width:480px; margin-top:-10px; ">
	<div>	
	<div class="seccion"><label for="tel">*Telefono fijo:</label><br><input type="tel" class="caja"  placeholder="Telefono casa/oficina"  id="tel" name="tel" required /></div>
	<div class="seccion"><label for="cel">Movil:</label><br><input type="tel"  class="caja" placeholder="Celular"  id="cel" name="cel"   /></div>
	</div>
	<div>	
	<div class="seccion"><label for="email">*E-mail:</label><br><input type="email" class="caja" placeholder="correo electronico" id="email" name="email"  autocomplete="off" required /></div>
	<div class="seccion"><label for="email_alternativo">E-mail alternativo:</label><br><input type="email"  class="caja"  placeholder="correo alternativo" id="email_alternativo" name="email_alternativo" autocomplete="off"  /></div>
</div>
</div>

	
	<div style=" font-family:Tahoma; font-size:11px; float:left; width:280px; margin-top:-80px; margin-left:470px;">
		<div><input type="checkbox" style="width:10px; margin-right:5px;"  checked="checked" valule="ok"  name="news"/><label for="news">Quiero recibir ofertas, invitaciones y promociones.</label></div>
		<div><input type="checkbox"  style="width:10px; margin-right:5px;" checked="checked" valule="ok" name="terminos" required/><label for="terminos">Acepto los <a target="_blank" href="<?= base_url('legal/terminos_y_condiciones') ?>">Términos y condiciones de uso del Programa ADDICTED</a></label></div>
		<div><input type="checkbox"  style="width:10px; margin-right:5px;" checked="checked" valule="ok" name="terminos" required/><label for="privacy">Acepto el <a target="_blank" href="<?= base_url('legal/aviso_de_privacidad') ?>">Aviso de privacidad</a></label></div>
	</div>


           <div style=" margin-top:130px; margin-right:50px; background-color:#658a43; width:100%; height:50px; text-align:right;"><input type="submit" id="btn_enviar" class="boton_envio" name="Enviar" value="Preregístro" /></div>


</form>

</div>
</div>