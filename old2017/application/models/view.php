<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Model {


	public function set($str)
	{
		$this->load->view('theme/header');
		$this->load->view($str);
		$this->load->view('theme/footer');
		
	}
}
