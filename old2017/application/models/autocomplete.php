<?php

class autocomplete extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    public function get_cnx() {
        $con = mysql_connect("localhost", 'mca_concierge', '3cr#thUs+Wr=!2kT');
        mysql_select_db("mx_com_addicted_postal_services", $con);
        return $con;
    }

    public function municipios($estado, $asArray = false) {
        $con = $this->get_cnx();
        $result = mysql_query("SELECT municipio FROM municipi WHERE estado = '" . $estado . "' ORDER BY municipio ASC");
        if (mysql_num_rows($result) > 0) {
            $res = $asArray ? array() : '';
            while ($row = mysql_fetch_array($result)) {
                if (is_array($res)) {
                    array_push($res, $row['municipio']);
                } else {
                    $res .= '<option>' . $row['municipio'] . '</option>';
                }
            }
            mysql_close($con);
            return $res;
        }
        mysql_close($con);
    }

    public function colonias($municipio, $asArray = false) {
        $con = $this->get_cnx();
        $result = mysql_query("SELECT id, colonia FROM colonias WHERE municipio = '" . $municipio . "' ORDER BY colonia ASC");
        if (mysql_num_rows($result) > 0) {
            $res = $asArray ? array() : '';
            while ($row = mysql_fetch_array($result)) {
                if (is_array($res)) {
                    $res[$row['colonia']] = $row['colonia'];
                } else {
                    $res .= '<option value="' . $row['id'] . '">' . $row['colonia'] . "</option>";
                }
            }
            mysql_close($con);
            return $res;
        }
        mysql_close($con);
    }

    public function cp($id) {
        $con = $this->get_cnx();
        $result = mysql_query("SELECT cp FROM cps WHERE id = " . $id) or die(mysql_error());
        $res = '';
        if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
                $res .= $row['cp'];
            }
        }
        mysql_close($con);
        return $res;
    }
}
