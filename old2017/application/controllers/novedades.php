<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class novedades extends CI_Controller {
	
	public function index()
	{
		$this->view->set('novedades',array('id'=>null));
	}
	
	public function detalle($id = null)
	{
		$this->view->set('novedades',array('id'=>$id));
	}
}