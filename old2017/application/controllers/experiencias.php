<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class experiencias extends CI_Controller {
	
	public function index()
	{
		$this->view->set('experiencias',array('id'=>null));
	}
	
	public function detalle($id = null)
	{
		$this->view->set('experiencias',array('id'=>$id));
	}
}