<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Proceso extends CI_Controller {
	
	var $data_errors = array();
	
	public function index()
	{
		$this->load->helper('url');
		
		$data['firstname']=$this->input->post('nombre');
		$data['lastname1'] = $this->input->post('apeP');
		$data['lastname2'] = $this->input->post('apeM');
		$data['gender'] = $this->input->post('sexo');
		$data['birthday'] = $this->input->post('nacimiento');
		$data['estado'] = $this->input->post('estado');
		$data['municipio'] = $this->input->post('municipio');
		$data['colonia'] = $this->input->post('colonia');
		$data['postal_code'] = str_replace('Código postal: ','',$this->input->post('postal_code'));
		$data['street'] = $this->input->post('calle');
		$data['exterior'] = $this->input->post('exterior');
		$data['interior']= $this->input->post('interior');
		$data['telephone'] = $this->input->post('tel');
		$data['mobile'] = $this->input->post('cel');
		$data['email'] = $this->input->post('email');
		$data['email_alternativo'] = $this->input->post('email_alternativo');
		$data['news'] = isset($_POST['news']) ? 'ok':'no';
		
		if($this->process($data))
		{
			$this->session->set_flashdata('notification','Su preregístro addicted se ha realizado con éxito, en su siguiente visita pase a Concierge.');
			redirect(base_url());
		}
		else
		{
			$this->session->set_flashdata('notification',implode('<br>',$this->data_errors));
			redirect(base_url());
		}
		
		
	}
	
	private function process($data)
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('firstname', 'Nombre', 'required');
		$this->form_validation->set_rules('apeM', 'Apellido materno', 'required');
		$this->form_validation->set_rules('apeP', 'Nombre', 'required');
		$this->form_validation->set_rules('email', 'e-mail', 'required|valid_email');
		$this->form_validation->set_rules('email_alternativo', 'e-mail alternativo', 'required|valid_email');
		
		if($_POST['estado'] == 'Seleccione'){$this->form_validation->set_message('estado_check','Error en campo estado');}
		if($_POST['municipio'] == 'Seleccione'){$this->form_validation->set_message('municipio_check','Error en campo municipio');}
		if($_POST['colonia'] == 'Seleccione'){$this->form_validation->set_message('colonia_check','Error en campo colonia');}
		
		if($this->form_validation->run() == FALSE)
		{
			return $this->db->insert('preregistro',$data);
		}
		else
		{
			$this->data_errors = explode('[|||]',validation_errors('[|||]',''));
			return false;
		}
		
	}
}