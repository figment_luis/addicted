<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class socio extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function acceso($data = null)
	{
		if($data == null)
		{
			redirect('http://concierge.addicted.com.mx/addicted/setpass','refresh');
		}
		else
		{
			redirect('http://concierge.addicted.com.mx/addicted/establecer/'.$data,'refresh');
		}
	}
	
	public function t($email)
	{
		echo base64_encode($email.'@n7@7@');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */