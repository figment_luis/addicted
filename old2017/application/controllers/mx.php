<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mx extends CI_Controller
{
	public function index()
    {
	    //
    }
	
	public function municipios()
	{
		if(isset($_POST['estado']))
		{
			$this->load->model('autocomplete');
			header('Access-Control-Allow-Origin: *');
			echo $this->autocomplete->municipios($this->input->post('estado'));
		}
	}
	
	public function colonias()
	{
		if(isset($_POST['municipio']))
		{
			$this->load->model('autocomplete');
			header('Access-Control-Allow-Origin: *');
			echo $this->autocomplete->colonias($this->input->post('municipio'));
		}
	}
	
	public function cp()
	{
		if(isset($_POST['id']))
		{
			$this->load->model('autocomplete');
			header('Access-Control-Allow-Origin: *');
			echo $this->autocomplete->cp($this->input->post('id'));
		}
	}
}