<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class descuentos extends CI_Controller {
	
	public function index()
	{
		$this->view->set('descuentos',array('id'=>null));
	}
	
	public function detalle($id = null)
	{
		$this->view->set('descuentos',array('id'=>$id));
	}
}