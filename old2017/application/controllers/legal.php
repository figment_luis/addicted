<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class legal extends CI_Controller {

	public function terminos_y_condiciones()
	{
		$this->load->view('terminos_y_condiciones');
	}
	
	public function aviso_de_privacidad()
	{
		$this->load->view('aviso_de_privacidad');
	}
}
