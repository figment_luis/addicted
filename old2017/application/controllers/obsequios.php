<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class obsequios extends CI_Controller {
	
	public function index()
	{
		$this->view->set('obsequios',array('id'=>null));
	}
	
	public function detalle($id = null)
	{
		$this->view->set('obsequios',array('id'=>$id));
	}
}