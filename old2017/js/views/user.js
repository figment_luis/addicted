function view_init()
{
	$('#estado').dropkick({theme:'black',change:estadoChangeHandler});
	
	if(isVisible('#municipio'))
	{
		$('#municipio').dropkick({theme:'black',change:municipioChangeHandler});
	}
	
	if(isVisible('#colonia'))
	{
		$('#colonia').dropkick({theme:'black',change:coloniaChangeHandler});
	}
}

function add_validation()
{
	
}

function estadoChangeHandler(value,label)
{
	if(value != 'Seleccione')
	{
		$.post('http://beta.kinetiq.mx/mx/municipios/',{estado:value},function(data)
		{
			$('#municipio').removeData("dropkick");
			$("#dk_container_municipio").remove();
			
			$('#municipio').html('<option>Seleccione</option>' + data);
			$('#municipio').dropkick({theme:'black',change:municipioChangeHandler});
			$('[for="municipio"]').show();
			
			$('#colonia').removeData("dropkick");
			$("#dk_container_colonia").remove();
			$('[for="colonia"]').hide();
			
			$('#ccp').animate({opacity:'0'});
			$('[for="postal_code"]').hide();
		});
	}
	else
	{
		$('#municipio').removeData("dropkick");
		$("#dk_container_municipio").remove();
		$('[for="municipio"]').hide();
		
		$('#colonia').removeData("dropkick");
		$("#dk_container_colonia").remove();
		$('[for="colonia"]').hide();
		
		$('#ccp').animate({opacity:'0'});
		$('[for="postal_code"]').hide();
	}
	setLabels();
}

function municipioChangeHandler(value,label)
{
	if(value != 'Seleccione')
	{
		$.post('http://beta.kinetiq.mx/mx/colonias/',{municipio:value},function(data)
		{
			$('#colonia').removeData("dropkick");
			$("#dk_container_colonia").remove();
			
			$('#colonia').html('<option>Seleccione</option>' + data);
			$('#colonia').dropkick({theme:'black',change:coloniaChangeHandler});
			$('[for="colonia"]').show();
			
			$('#ccp').animate({opacity:'0'});
			$('[for="postal_code"]').hide();
		});
	}
	else
	{
		$('#colonia').removeData("dropkick");
		$("#dk_container_colonia").remove();
		$('[for="colonia"]').hide();
		
		$('#ccp').animate({opacity:'0'});
		$('[for="postal_code"]').hide();
	}
	setLabels();
}

function coloniaChangeHandler(value,label)
{
	if(value != 'Seleccione')
	{
		$.post('http://beta.kinetiq.mx/mx/cp/',{id:value},function(data)
		{
			$('#ccp').val('Código postal: ' + data);
			
			$('#ccp').animate({opacity:'1'});
			$('[for="postal_code"]').show();
		});
	}
	else
	{
		$('#ccp').animate({opacity:'0'});
		$('[for="postal_code"]').hide();
	}
	setLabels();
}