$(document).ready(function()
{
	
});

function set_mod(e,o)
{
	$(e).addClass('item_active');
	$('.item').not(e).removeClass('item_active');
	
	$("html, body").animate({scrollTop: $('body').offset().top },'slow',function()
	{
		$('#n_selected').slideUp('fast',function()
		{
			$('#n_img').css('background-image','url(' + o.img + ')');
			$('#n_title').html(o.title);
			$('#n_valor').html(o.puntos);
			$('#n_txt').html(o.content);
			$('#n_url').bind('click',function(){window.location = o.url;});
			$('#n_selected').slideDown('slow');
		});
	}); 

}