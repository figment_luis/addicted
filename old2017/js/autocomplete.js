$(document).ready(function()
{
    $.support.cors = true;
    $('#estado').change(estadoChangeHandler);
    $('#municipio').change(municipioChangeHandler);
    $('#colonia').change(coloniaChangeHandler);
});

function estadoChangeHandler()
{
    console.log('estadoChangeHandler reached');
    if ($('#estado').val() != 'Seleccione')
    {
        $.post('/mx/municipios', {estado: $('#estado').val()}, function(data)
        {
            $('#municipio').html('<option>Seleccione</option>' + data);
            $('.municipio').show();
            $('.colonia').hide();
            $('.ccp').animate({opacity: '0'});
        });
    }
    else
    {
        $('.municipio').hide();
        $('.colonia').hide();
        $('.ccp').animate({opacity: '0'});
    }
}

function municipioChangeHandler()
{
    console.log('municipioChangeHandler reached');
    if ($('#municipio').val() != 'Seleccione')
    {
        $.post('/mx/colonias', {municipio: $('#municipio').val()}, function(data)
        {
            $('#colonia').html('<option>Seleccione</option>' + data);
            $('.colonia').show();
            $('.ccp').animate({opacity: '0'});
        });
    }
    else
    {
        $('.colonia').hide();
        $('.ccp').animate({opacity: '0'});
    }
}

function coloniaChangeHandler()
{
    console.log('estadoChangeHandler reached');
    if ($('#colonia').val() != 'Seleccione')
    {
        $.post('/mx/cp', {id: $('#colonia').val()}, function(data)
        {
            $('#ccp').val('Código postal: ' + data);
            $('.ccp').animate({opacity: '1'});
        });
    }
    else
    {
        $('.ccp').animate({opacity: '0'});
    }
}