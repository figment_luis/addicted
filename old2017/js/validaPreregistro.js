$(document).ready(function(){

	//variables globales

	var inputUsername = $("#nombre");	
	var reqUsername = $("#req-nombre");
	
	var inputApeP = $("#apeP");	
	var reqApeP = $("#req-apeP");
	
	var inputApeM = $("#apeM");	
	var reqApeM = $("#req-apeM");
	
	var inputEstado = $("#estado");	
	var reqEstado = $("#req-estado");
	
	var inputMunicipio = $("#municipio");	
	var reqMunicipio = $("#req-municipio");
	
	var inputColonia = $("#colonia");	
	var reqColonia = $("#req-colonia");
	
	var inputPostal = $("#postal_code");	
	var reqPostal = $("#req-postal_code");
	
	var inputCalle = $("#calle");	
	var reqCalle = $("#req-calle");
	
	var inputExterior = $("#exterior");	
	var reqExterior = $("#req-exterior");
	
	var inputInterior = $("#interior");	
	var reqInterior = $("#req-interior");
	
	var inputTel = $("#tel");	
	var reqTel = $("#req-tel");
	
	var inputCel = $("#cel");	
	var reqCel = $("#req-cel");
		
	var inputEmail = $("#email");
	var reqEmail = $("#req-email");
	
	var inputEmailDos = $("#email_alternativo");	
	var reqEmailDos = $("#req-email_alternativo");
	



	//funciones de validacion
	function validateUsername(){
		//NO cumple longitud minima
		if(inputUsername.val().length < 2){
			reqUsername.addClass("error");
			inputUsername.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputUsername.val().match(/^[a-zA-Z]+$/)){
			reqUsername.addClass("error");
			inputUsername.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqUsername.removeClass("error");
			inputUsername.removeClass("error");
			return true;
		}
	}
	

function validateApeP(){
		//NO cumple longitud minima
		if(inputApeP.val().length < 2){
			reqApeP.addClass("error");
			inputApeP.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputApeP.val().match(/^[a-zA-Z]+$/)){
			reqApeP.addClass("error");
			inputApeP.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqApeP.removeClass("error");
			inputApeP.removeClass("error");
			return true;
		}
	}
	
/*
function validateApeM(){
		//NO cumple longitud minima
		if(inputApeM.val().length < 4){
			reqApeM.addClass("error");
			inputApeM.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputApeM.val().match(/^[a-zA-Z]+$/)){
			reqApeM.addClass("error");
			inputApeM.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqApeM.removeClass("error");
			inputApeM.removeClass("error");
			return true;
		}
	}
*/

function validateEstado(){
		//NO cumple longitud minima
		if(inputEstado.val().length < 4){
			reqEstado.addClass("error");
			inputEstado.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputEstado.val().match(/^[a-zA-Z]+$/)){
			reqEstado.addClass("error");
			inputEstado.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqEstado.removeClass("error");
			inputEstado.removeClass("error");
			return true;
		}
	}	
	

function validateCalle(){
		//NO cumple longitud minima
		if(inputCalle.val().length < 4){
			reqCalle.addClass("error");
			inputCalle.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputCalle.val().match(/^[a-zA-Z0-9]+$/)){
			reqCalle.addClass("error");
			inputCalle.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqCalle.removeClass("error");
			inputCalle.removeClass("error");
			return true;
		}
	}

	
function validateExterior(){
		//NO cumple longitud minima
		if(inputExterior.val().length < 4){
			reqExterior.addClass("error");
			inputExterior.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputExterior.val().match(/^[0-9]+$/)){
			reqExterior.addClass("error");
			inputExterior.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqExterior.removeClass("error");
			inputExterior.removeClass("error");
			return true;
		}
	}
	
/* 
function validateInterior(){
		//NO cumple longitud minima
		if(inputInterior.val().length < 4){
			reqInterior.addClass("error");
			inputInterior.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputInterior.val().match(/^[a-zA-Z]+$/)){
			reqInterior.addClass("error");
			inputInterior.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqInterior.removeClass("error");
			inputInterior.removeClass("error");
			return true;
		}
	}
	 */
	
function validateTel(){
		//NO cumple longitud minima
		if(inputTel.val().length < 4){
			reqTel.addClass("error");
			inputTel.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputTel.val().match(/^[0-9]+$/)){
			reqTel.addClass("error");
			inputTel.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqTel.removeClass("error");
			inputTel.removeClass("error");
			return true;
		}
	}
	

		
/* function validateCel(){
		//NO cumple longitud minima
		if(inputCel.val().length < 4){
			reqCel.addClass("error");
			inputCel.addClass("error");
			return false;
		}
		//SI longitud pero NO solo caracteres A-z
		else if(!inputCel.val().match(/^[a-zA-Z]+$/)){
			reqCel.addClass("error");
			inputCel.addClass("error");
			return false;
		}
		// SI longitud, SI caracteres A-z
		else{
			reqCel.removeClass("error");
			inputCel.removeClass("error");
			return true;
		}
	} */
	
	
function validateEmail(){
		//NO hay nada escrito
		if(inputEmail.val().length == 0){
			reqEmail.addClass("error");
			inputEmail.addClass("error");
			return false;
		}
		// SI escrito, NO VALIDO email
		else if(!inputEmail.val().match(/^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i)){
			reqEmail.addClass("error");
			inputEmail.addClass("error");
			return false;
		}
		// SI rellenado, SI email valido
		else{
			reqEmail.removeClass("error");
			inputEmail.removeClass("error");
			return true;
		}
	}
	
	
/* function validateEmailDos(){
		//NO hay nada escrito
		if(inputEmailDos.val().length == 0){
			reqEmailDos.addClass("error");
			inputEmailDos.addClass("error");
			return false;
		}
		// SI escrito, NO VALIDO email
		else if(!inputEmailDos.val().match(/^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i)){
			reqEmailDos.addClass("error");
			inputEmailDos.addClass("error");
			return false;
		}
		// SI rellenado, SI email valido
		else{
			reqEmailDos.removeClass("error");
			inputEmailDos.removeClass("error");
			return true;
		}
	} */
	
	

	//controlamos la validacion en los distintos eventos
	// Perdida de foco
	inputUsername.blur(validateUsername);
	inputApeP.blur(validateApeP);
	//inputApeM.blur(validateApeM);
	inputEstado.blur(validateEstado);

	inputCalle.blur(validateCalle);
	inputExterior.blur(validateExterior);
	//inputInterior.blur(validateInterior);
	inputTel.blur(validateTel);
	//inputCel.blur(validateCel);
	//inputEmailDos.blur(validateEmailDos);
	inputEmail.blur(validateEmail);
	
	// Pulsacion de tecla
	inputUsername.keyup(validateUsername);
	inputApeP.keyup(validateApeP);
	//inputApeM.keyup(validateApeM);
	inputEstado.keyup(validateEstado);

	inputCalle.keyup(validateCalle);
	inputExterior.keyup(validateExterior);
	//inputInterior.keyup(validateInterior);
	inputTel.keyup(validateTel);
	//inputCel.keyup(validateCel);
	//inputEmailDos.keyup(validateEmailDos);
	inputEmail.keyup(validateEmail);

		
// Envio de formulario
	$("#preRegistro").submit(function(){
		if(validateUsername() & validateApeP() &  validateEstado() &  validateCalle() & validateExterior() & vvalidateTel() & validateEmail())
			return true;
		else
			return false;
	});

});


