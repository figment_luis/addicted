$(document).ready(function(){
	//variables globales
	
	var inputEmail = $("#mail");
	var reqEmail = $("#req-email");
	
	var inputPassword1 = $("#password");
	var reqPassword1 = $("#req-password1");

	
		function validateEmail(){
		//NO hay nada escrito
		if(inputEmail.val().length == 0){
			reqEmail.addClass("error");
			inputEmail.addClass("error");
			return false;
		}
		// SI escrito, NO VALIDO email
		else if(!inputEmail.val().match(/^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i)){
			reqEmail.addClass("error");
			inputEmail.addClass("error");
			return false;
		}
		// SI rellenado, SI email valido
		else{
			reqEmail.removeClass("error");
			inputEmail.removeClass("error");
			return true;
		}
	}
	

	function validatePassword1(){
		//NO tiene minimo de 5 caracteres o mas de 12 caracteres
		if(inputPassword1.val().length <2 || inputPassword1.val().length > 100){
			reqPassword1.addClass("error");
			inputPassword1.addClass("error");
			return false;
		}
		// SI longitud, NO VALIDO numeros y letras
/*
		else if(!inputPassword1.val().match(/^[0-9a-zA-Z]+$/)){
			reqPassword1.addClass("error");
			inputPassword1.addClass("error");
			return false;
		}
*/
		// SI rellenado, SI email valido
		else{
			reqPassword1.removeClass("error");
			inputPassword1.removeClass("error");
			return true;
		}
	}
	

	//controlamos la validacion en los distintos eventos
	// Perdida de foco

	//inputEmail.blur(validateEmail);
	//inputPassword1.blur(validatePassword1);  

		// Envio de formulario
	$("#formLogin").submit(function(){
		if(validatePassword1() & validateEmail())
			return true;
		else
			return false;
	});




});