<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| Variables propias
| -------------------------------------------------------------------
|
*/

//datos de la promo actual
$config['micrositio_name'] = "";
$config['micrositio_landing'] = "";  //la img dentro del micrositio
$config['popup_micrositio_desktop_img'] = "assets/img/popup/desktop.jpg";  //la img del popup desktop
$config['popup_micrositio_mobile_img'] = "assets/img/popup/mobile.jpg";  //la img del popup mobile
$config['popup_micrositio_start'] = "2020-06-01 00:00:00";
$config['popup_micrositio_end'] = "2020-09-30 23:59:59";
$config['popup_micrositio_link'] = "";
$config['micrositio_tracking'] = "";
