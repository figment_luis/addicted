<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Experiencias extends CI_Controller {

	public function index()
	{
        $data['experiencias'] = $this->contenido->get_all('experiencias');
        $this->load->view('header');
        $this->load->view('experiencias',$data);
        $this->load->view('footer');
	}
 
	
}