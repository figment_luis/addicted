<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Beneficios extends CI_Controller {

	public function index()
	{
        $data['beneficios'] = $this->contenido->get_all('beneficios');
        $this->load->view('header');
        $this->load->view('beneficios',$data);
        $this->load->view('footer');
	}
 
	
}