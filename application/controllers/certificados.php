<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Certificados extends CI_Controller {

	public function index()
	{
        $data['certificados'] = $this->contenido->get_all('obsequios');
        $this->load->view('header');
        $this->load->view('certificados',$data);
        $this->load->view('footer');
	}
 
	
}