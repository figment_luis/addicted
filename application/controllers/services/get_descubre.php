<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Get_descubre extends CI_Controller
{
	function __construct() {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
        parent::__construct();
    }

    
	public function index()
	{
		$this->load->model('services');

		$response = $this->services->get_descubre();
				
		echo json_encode($response);
	}

}

?>