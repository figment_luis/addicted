<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Get_benefits extends CI_Controller
{
	function __construct() {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
        parent::__construct();
    }

    
	public function index()
	{
		$this->load->model('services');

		$beneficios = $this->services->get_beneficios();
		$descuentos = $this->services->get_descuentos();		
		$response = array_merge($descuentos, $beneficios);

		foreach ($response as $key => $row) {
		    $aux[$key] = $row['title'];
		}

		// array_multisort($aux, SORT_ASC, $response);
		
		echo json_encode($response);
	}

}

?>