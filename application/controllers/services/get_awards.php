<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class Get_awards extends CI_Controller
{
	function __construct() {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
        parent::__construct();
    }

    
	public function index()
	{
		$this->load->model('services');

		// $experiencias = $this->services->get_experiencias(null);
		// $obsequios = $this->services->get_obsequios(null);		
		// $response = array_merge($obsequios, $experiencias);
		

		// foreach ($response as $key => $row) {
		//     $aux[$key] = $row['puntos'];
		// }

		$response = $this->contenido->get_all('obsequios');
		// array_multisort($aux, SORT_ASC, $response);
		
		echo json_encode($response);
	}

	public function test(){
		$this->load->model('services');
		$data = $this->contenido->get_all('obsequios');
		echo json_encode($data);
	}
	
	public function title()
	{
		$this->load->model('services');

		$experiencias = $this->services->get_experiencias('title');
		$obsequios = $this->services->get_obsequios('title');		
		$response = array_merge($obsequios, $experiencias);

		foreach ($response as $key => $row) {
		    $aux[$key] = $row['puntos'];
		}

		// array_multisort($aux, SORT_ASC, $response);
		
		echo json_encode($response);
	}

}

?>