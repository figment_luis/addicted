<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class preregister extends CI_Controller
{
	function __construct() {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $method = $_SERVER['REQUEST_METHOD'];
    if($method == "OPTIONS") {
        die();
    }
        parent::__construct();
    }

    
	public function index()
	{
		$response['status'] = false;
		$response['msg'] = "Acceso no autorizado.";
		echo json_encode($response);
		return false;
    }
    
    public function user($name = false, $lastname = false, $lastnamem = false, $email = false)
    {
        if($name == false || $lastname == false  ||  $lastnamem == false ||  $email== false){
            $response['status'] = false;
            $response['msg'] = "Acceso no autorizado.";
            echo json_encode($response);
            return false;
        }
        else{
            $data = array(
                'name' => $name,
                'lastname' => $lastname,
                'lastnamem' => $lastnamem,
                'email' => str_replace('+', "@", $email)   
            );
            $this->load->model('services');
            $result = $this->services->preregister($data);
            echo json_encode($result);
        }
    }

}
?>