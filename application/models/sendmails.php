<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class sendmails extends CI_Model {
    private function mailSettings(){
        //Config Amazon
        $config = [];
        $config['mailtype']     = 'html';
        $config['priority']     = 3;
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = 's182900.gridserver.com';
        $config['smtp_port']    = 587;
        $config['smtp_crypto']  = '';
        $config['smtp_user']    = 'donotreply@kinetiq.com.mx';
        $config['smtp_pass']    = 'twMA_Vpki5c754xH';
        $config['smtp_timeout'] = 20;
        $config['newline']      = "\r\n";

        return $config;
    }

    public function mailPreregister($email,$name){
        $email = str_replace('+', "@", $email);
        //Load Config Amazon
        $config = $this->mailSettings();
        //Setting Headers Mail
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->print_debugger();
        $this->email->from('concierge@addicted.com.mx', 'Addicted');
        $this->email->to($email);
        $this->email->subject('Pre-registro');
        //Template de mail
        $this->email->message('<table width="600" border="0" align="center"> <tr> <td align="center" valign="middle" class="logoTd"> <img class="off" src="http://addicted.com.mx/assets/img/logo_addicted_black.png" width="250"> </td></tr><tr> <td bgcolor="#ffffff" colspan="3"> <img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/shortShadow.jpg" width="100%" height="28" border="0" alt=""> </td></tr><tr> <td class="mobileHeadline" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:28px;padding:0 25px 5px 24px;font-weight:700">Gracias por tu pre registro.</td></tr><tr> <td class="mobileCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:16px;padding:0 25px 30px 24px;line-height:24px"> <strong>Hola '. str_replace('/', '', $name) .',</strong> </td></tr><tr> <td class="tdDash" style="width:100%;height:10px;padding:0 0 30px 0"> <img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/dash.jpg" width="100%" height="10" border="0" alt=""> <span class="mobileDash"></span> </td></tr><tr> <td class="mobileCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:16px;padding:0 25px 30px 24px;line-height:24px">Tu pre registro ha sido un éxito, te invitamos a que nos visites en el módulo de concierge en Antara para entregarte tu nueva tarjeta y comenzar a disfrutar de ser un socio del plan de lealtad Addicted. <br><br>Tu correo registrado es: <strong> '.$email.' </strong> <br><br>Te esperamos en el módulo de concierge. </td></tr><tr> <td class="tdDash" style="width:450px;height:10px;padding:0 0 30px 0"> <img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/dash.jpg" width="100%" height="10" border="0" alt=""> <span class="mobileDash"></span> </td></tr><tr> <td align="center" class="mobileCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:16px;padding:0 25px 30px 24px;line-height:24px"> <p>Atentamente <br><img class="off" src="http://addicted.com.mx/assets/img/logo_contacto.png"> </p></td></tr><tr> <td bgcolor="#ffffff" colspan="0"> <img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/shortShadow.jpg" width="100%" height="28" border="0" alt=""> </td></tr><tr> <td class="mobileFooterCopy " style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#666;font-size:12px;padding:0 15px 2px 15px;line-height:16px ">El uso del sitio web y del servicio de Addicted constituye la aceptación de los <a href="http://addicted.com.mx/terminosycondiciones" target="_blank " style="color:#666 ">Términos de uso</a> y de la <a style="color:#666 " href="http://addicted.com.mx/avisodeprivacidad">Política de privacidad</a>.</td></tr><tr> <td class="mobileFooterCopy " style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#666;font-size:12px;padding:0 15px 2px 15px;line-height:16px "> (c) Antara Fashion Hall.</td></tr></table>');
        //Test mail
        if ($this->email->send()){
          // return true;
        }
        else{
          // return false;
        }
    }

    public function mailRecoveryPass($email,$token){
        $email = str_replace('+', "@", $email);
        //Load Config Amazon
        $config = $this->mailSettings();
        //Setting Headers Mail
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->print_debugger();
        $this->email->from('concierge@addicted.com.mx', 'Addicted');
        $this->email->to($email);
        $this->email->subject('Recuperar contraseña');
        //Template de mail
        $this->email->message('<table width="600" border="0" align="center"> <tr><td align="center " valign="middle" class="logoTd"><img class="off" src="http://addicted.com.mx/assets/img/logo_addicted_black.png" width="250"></td></tr><tr><td bgcolor="#ffffff " colspan="3"><img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/shortShadow.jpg" width="100% " height="28 " border="0 " alt=" "></td></tr><tr><td class="mobileHeadline" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:28px;padding:0 25px 5px 24px;font-weight:700">Recuperación de contraseña.</td></tr><tr><td class="tdDash" style="width:100%;height:10px;padding:0 0 30px"><img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/dash.jpg" width="100%" height="10 " border="0 " alt=" "> <span class="mobileDash"></span></td></tr><tr><td class="mobileCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:16px;padding:0 25px 30px 24px;line-height:24px">Tiene 3 días para poder cambiar su contraseña, después de ese tiempo el sistema bloqueará el enlace y tendrá que volver a solicitar la recuperación de contraseña. Recuerde que la contraseña distingue entre mayúsculas y minúsculas.<br><br>Click en el siguiene enlace para recuperar su contraseña:<br><br><a href="http://concierge.addicted.com.mx/services/get_passwordRecovery/recoverypassword?token='.$token.'" target="_blank">http://concierge.addicted.com.mx/services/get_passwordRecovery/recoverypassword?token='.$token.'</a></td></tr><tr><td class="tdDash" style="width:450px;height:10px;padding:0 0 30px"><img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/dash.jpg" width="100% " height="10 " border="0 " alt=" "> <span class="mobileDash"></span></td></tr><tr><td align="center" class="mobileCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#454545;font-size:16px;padding:0 25px 30px 24px;line-height:24px"><p>Atentamente<br><img class="off" src="http://addicted.com.mx/assets/img/logo_contacto.png"></p></td></tr><tr><td bgcolor="#ffffff " colspan="0"><img class="off" style="display:block" src="http://www.addicted.com.mx/assets/img/shortShadow.jpg" width="100% " height="28 " border="0 " alt=""></td></tr><tr><td class="mobileFooterCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#666;font-size:12px;padding:0 15px 2px;line-height:16px">El uso del sitio web y del servicio de Addicted constituye la aceptación de los <a href="http://addicted.com.mx/terminosycondiciones" target="_blank " style="color:#666">Términos de uso</a> y de la <a style="color:#666" href="http://addicted.com.mx/avisodeprivacidad">Política de privacidad</a>.</td></tr><tr><td class="mobileFooterCopy" style="font-family:Arial,Helvetica,sans-serif;-webkit-text-size-adjust:none;color:#666;font-size:12px;padding:0 15px 2px;line-height:16px">(c) Antara Fashion Hall.</td></tr></table></body></html>');
        //Test mail
        if ($this->email->send()){
          // return true;
        }
        else{
          // return false;
        }
    }

}