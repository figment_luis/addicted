<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contenido extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function get_all($object)
    {
        $consulta = $object . ' WHERE enabled = 1';
        if ($object == 'obsequios' || $object == 'experiencias' ){
            $consulta = $object . ' WHERE enabled = 1 ORDER BY puntos ASC';
        }
        $query = $this->db->query("SELECT * FROM $consulta");
        
        if($query->num_rows() > 0)
        {
             return $query->result_array();
        }
        else{
            return false;
        }   
    }
}