<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contenido extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function get_all($object)
    {
        $query = $this->db->query("SELECT * FROM $object");
        
        if($query->num_rows() > 0)
        {
             return $query->result_array();
        }
        else{
            return false;
        }   
    }
}