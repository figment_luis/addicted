<div class="interiores">
    <div class="block-slide">


        <div class="cycle-slideshow" data-cycle-timeout="3000" data-cycle-slides="> div">
            <div style="background-image:url(<?= base_url('assets/img/backgrounds/Descubre-1.jpg') ?>);" class="img-responsive img-slide">
                <p>
            </div>
    
        </div>

        <div class="clearfix"></div>

        <img class="logo-slide hidden" src="<?= base_url('assets/img/logo_addicted.png') ?>" alt="Addicted">
        <div class="title-content-xs visible-xs">
            <hr>
            <h1 class="light">DESCUBRE</h1>
<!--            <p class="light">Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios y beneficios.</p>-->
        </div>
    </div>
    <div class="block-content descubre">
        <div class="title-content hidden-xs">DESCUBRE</div>
        <h4>¿QUÉ ES?</h4>
        <div>
            Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios como:
            <ul>
                <li>Viajes para presenciar las entregas de los premios más afamados, como los Oscar y Grammy.</li>
                <li>Boletos a los mejores conciertos al rededor del mundo, con pases VIP y al backstage.</li>
                <li>Viajes a los destinos más exclusivos. </li>
                <li>Acceso a los eventos de moda de mayor prestigio, como al Fashion Week en Paris o Nueva York.</li>
            </ul>
        </div>
        <div class="title-descubre green">.. Y LOS MEJORES BENEFICIOS COMO:</div>
        <ul>
            <li>Estacionamiento gratuito por 2 hrs una vez al día dentro de Antara Fashion Hall.</li>
            <li>Descuentos y promociones especiales con nuestras marcas aliadas.</li>
            <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
        </ul>
        <div>
            <img src="<?= base_url('assets/img/4_pasos-1.png'); ?>" class="img-responsive img-pasos">
        </div>
        <ul class="pasos">
            <li style="background-image: url(<?= base_url('assets/img/01.png'); ?>);" class="icono-pasos">
                Inscríbete al mejor Plan de Lealtad con el Concierge dentro de Antara.
                <p/>
            </li>
            <li style="background-image: url(<?= base_url('assets/img/02.png'); ?>);" class="icono-pasos">
                Realiza tus compras en Antara.
                <p/>
            </li>
            <li style="background-image: url(<?= base_url('assets/img/03.png'); ?>);" class="icono-pasos">
                Presenta tu ticket de compra en nuestro Concierge para acumular puntos addicted.
                <p/>
            </li>
            <li style="background-image: url(<?= base_url('assets/img/04.png'); ?>);" class="icono-pasos">
                Intercambia tus puntos por los más asombrosos premios.
                <p/>
            </li>
        </ul>
        <div>Es muy fácil empezar a recibir los beneficios addicted</div>
        <ul>
            <li>Por tan solo 50 pesos de compra usted obtendrá puntos addicted.</li>
            <li>Presente sus tickets de compra y consumos en Concierge Antara, localizado en la planta baja de Antara Fashion Hall, donde se le entregará el Welcome Kit Addicted y se activará su tarjeta de membresía.</li>
            <li>En cada visita posterior, pase nuevamente a Concierge con sus tickets de compra y consumos, para que le sean acreditados los puntos correspondientes a su membresía.</li>
            <li>Todos los socios addicted tienen beneficios desde el momento en que obtienen su tarjeta, ¡Comience a usarlos ya!.</li>
        </ul>
        <div class="title-descubre green">Niveles y sus beneficios:</div>
        <div class="nivel">
            <h3 class="title-descubre">CLEAR</h3>
            <hr>
            <ul>
                <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
            </ul>
            <img src="<?= base_url('assets/img/tarjetas/clear.png'); ?>" class="img-responsive tarjeta">
        </div>
        <div class="clearfix"></div>
        <div class="nivel">
            <h3 class="title-descubre">PLATINUM</h3>
            <hr>
            <ul>
                <li>Estacionamiento gratuito por 2 hrs una vez al día.*</li>
                <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
            </ul>
            <img src="<?= base_url('assets/img/tarjetas/platinum.png'); ?>" class="img-responsive tarjeta">
        </div>
        <div class="clearfix"></div>
        <div class="nivel">
            <h3 class="title-descubre">BLACK</h3>
            <hr>
            <ul>
                <li>Servicio gratuito de Personal Shopper Antara</li>
                <li>Estacionamiento gratuito por 2 hrs una vez al día.*</li>
                <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
            </ul>
            <img src="<?= base_url('assets/img/tarjetas/black.png'); ?>" class="img-responsive tarjeta">
        </div>
        <div class="clearfix"></div>
        <div>
            <p>* Beneficio exclusivo para socios Platinum y Black con compras acreditadas en los últimos 30 días.</p>
            <p>En todos los beneficios aplican condiciones y reestricciones. Consultar directamente en Concierge Antara.<p>
            <p>Si usted acumula más puntos de los indicados en un nivel addicted y en el periodo</p>
            <p>de compras especificados, usted automáticamente obtendrá un upgrade al siguiente nivel addicted.</p>
        </div>
        <div class="footer-interiores">
            La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de  Antara Fashion Hall.
            <a href="http://addicted.com.mx/terminosycondiciones" target="_blank"><b>Términos y condiciones de uso del Programa ADDICTED</b></a> |
            <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank"><b>Aviso de Privacidad</b></a>
        </div>
    </div>
</div>