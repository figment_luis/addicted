<div class="interiores">
        <div class="block-slide">
            <div style="
    width: 100%;
    height: 100%;
    background: black;
"></div>
        </div>
    <div class="block-content">


          <p class="titulo1">DESCUBRE</p>
                    <p class="titulo2">¿QUÉ ES?</p>
                    <p class="parrafo1">Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios como:</p>
                    <ul>
                        <li> Viajes para presenciar las entregas de los premios más afamados, como los <strong>Oscar y Grammy.</strong>
                            <p/> </li>
                        <li> Boletos a los mejores conciertos al rededor del mundo, <strong>con pases VIP y al backstage.</strong>
                            <p/> </li>
                        <li> Viajes a los <strong>destinos más excusivos.</strong>
                            <br/>
                            <p/> </li>
                        <li> Acceso a los eventos de moda de mayor prestigio, como al <strong>Fashionweek rn Paris o Nueva York.</strong>
                            <p/> </li>
                    </ul>
                    <p class="titulo-verde">.. Y LOS MEJORES BENEFICIOS COMO:</p>
                    <ul>
                        <li>Estacionamiento gratis dentro de Antara Fashion Hall.</li>
                        <p/>
                        <li>Descuentos y promociones especiales con nuestras marcas aliadas.</li>
                        <p/>
                        <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                        <p/> </ul>
                    <br>
                    <br>
                    <br> <img src="<?= base_url('assets/img/4_pasos-1.png'); ?>" class="img-responsive">
                    <br>
                    <ul style="list-style: none;padding-left: 0px;">
                        <li style="background-image: url(<?= base_url('assets/img/01.png');?>);background-repeat: no-repeat;line-height: 50px;padding-left: 50px;"> Inscríbete al mejor Plan de Lealtad con el Concierge dentro de Antara. </li>
                        <li style="background-image: url(<?= base_url('assets/img/02.png');?>);background-repeat: no-repeat;line-height: 50px;padding-left: 50px;"> Realiza tus compras en Antara. </li>
                        <li style="background-image: url(<?= base_url('assets/img/03.png');?>);background-repeat: no-repeat;line-height: 50px;padding-left: 50px;"> Presenta tu ticket de compra en nuestro Concierge para acumular puntos addicted.</li>
                        <li style="background-image: url(<?= base_url('assets/img/04.png');?>);background-repeat: no-repeat;line-height: 50px;padding-left: 50px;"> Intercambia tus puntos por los más asombrosos premios. </li>
                    </ul>
                    <div id="detalles" style=" display: block;">
                        <a name="info"></a>
                        <p class="color" style="margin-left:20px; margin-top:20px;"> Es muy fácil empezar a recibir los beneficios addicted</p>
                        <ul id="descubre">
                            <li>Por tan solo 50 pesos de compra usted obtendrá puntos addicted.</li>
                            <li>Presente sus tickets de compra y consumos en Concierge Antara, localizado en la planta baja de Antara Fashion Hall, donde se le entregará el Welcome Kit Addicted y se activará su tarjeta de membresía.</li>
                            <li>En cada visita posterior, pase nuevamente a Concierge con sus tickets de compra y consumos, para que le sean acreditados los puntos correspondientes a su membresía.</li>
                            <li>Todos los socios addicted tienen beneficios desde el momento en que obtienen su tarjeta, ¡Comience a usarlos ya!.</li>
                        </ul>
                    </div>



                    <br><br>
                    <p class="titulo-verde" style="font-style: normal;"><br>
                       Niveles y sus beneficios:
                    </p>
                    <div style=" height:315px; margin-top:20px;">
                       <div style="float:left; width:600px;">
                          <p class="titulo2">Clear</p>
                          <ul id="descubre">
                             <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                             <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                             <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                             <li>Prueba de productos Kiehl's en Concierge.</li>
                          </ul>
                       </div>
                       <div style="float:left; margin-left:50px;"><img class="tarjeta" src="<?= base_url('assets/img/tarjetas/tarjeta_02.png'); ?>" class="img-responsive"></div>
                    </div>
                    <div style=" height:315px; margin-top:20px;">
                       <div style="float:left; width:600px;">
                          <p  class="titulo2">Platinum</p>
                          <ul id="descubre">
                             <li>Servicio gratuito de Personal Shopper Antara</li>
                             <li>Estacionamiento gratuito por 2 hrs una vez al día.</li>
                             <!--  <li>Una botella de agua "Smartwater" como cortesía para el socio y un acompañante.</li> -->
                             <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                             <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                             <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                             <li>Prueba de productos Kiehl's en Concierge.</li>
                          </ul>
                       </div>
                       <div style="float:left; margin-left:50px;"><img class="tarjeta" src="<?= base_url('assets/img/tarjetas/tarjeta_01.png'); ?>" class="img-responsive"></div>
                    </div>
                    <div style=" height:415px; margin-top:20px;">
                       <div style="float:left; width:600px;">
                          <p  class="titulo2">Black</p>
                          <ul id="descubre">
                             <li>Servicio gratuito de Personal Shopper Antara</li>
                             <li>Estacionamiento gratuito por 2 hrs una vez al día.</li>
                             <li>Cortesía de agua Evian para el socio y un acompañante.</li>
                             <!-- <li>Una botella de agua "Smartwater" como cortesía para el socio y un acompañante.</li> -->
                             <li>Invitación a eventos VIP en Antara Fashion Hall.</li>
                             <li>Invitación a ventas especiales en Antara Fashion Hall.</li>
                             <li>Descuentos y promociones especiales en nuestras marcas aliadas.</li>
                             <li>Prueba de productos Kiehl's en Concierge.</li>
                          </ul>
                       </div>
                       <div style="float:left; margin-left:50px;"><img class="tarjeta" src="<?= base_url('assets/img/tarjetas/tarjeta_03.png'); ?>" class="img-responsive"></div>
                    </div>
                    <div class="clearfix"></div>
                    <br><br>
                    <div style="text-align:center;">
                       <p>Si usted acumula más puntos de los indicados en un nivel addicted y en el periodo</p>
                       <p>
                          de compras especificados, usted automáticamente obtendrá un upgrade al siguiente nivel addicted.
                       </p>
                    </div>
                    <br><br>
                    <div class="footer-interiores">
                        La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco.
                        <a href="http://addicted.com.mx/terminosycondiciones" target="_blank"><b>Términos y condiciones de uso del Programa ADDICTED</b></a> |
                        <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank"><b>Aviso de Privacidad</b></a>
                    </div>
    </div>
</div>