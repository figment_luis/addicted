<div class="interiores">
    <div class="block-slide">


        <div class="cycle-slideshow" data-cycle-timeout="3000" data-cycle-slides="> div">
            <div style="background-image:url(<?= base_url('assets/img/backgrounds/premios_2.png') ?>);" class="img-responsive img-slide">
                <p>
            </div>
      
        </div>

        <div class="clearfix"></div>

        <img class="logo-slide hidden" src="<?= base_url('assets/img/logo_addicted.png') ?>" alt="Addicted">
        <div class="title-content-xs visible-xs">
            <hr>
            <h1 class="light">EXPERIENCIAS</h1>
<!--            <p class="light">Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios y beneficios.</p>-->
        </div>
    </div>
    <div class="block-content">
        <div class="title-content hidden-xs">EXPERIENCIAS</div>
        <?php  foreach ($experiencias as $row) : ?>
        <div class="media">
            <div class="media-left">
                <img  class="media-object" src="<?= base_url('assets/img/'.$row['image_url'])  ?>">
            </div>
            <div class="media-body">
                <h1><?= $row['title'] ?></h1>
                <p class="more"><?= $row['description'] ?></p>
            </div>
            <div class="puntos">Valor en puntos addicted: <?= number_format($row['puntos']); ?> puntos</div>
        </div>
        <?php endforeach; ?>
        <div class="footer-interiores">
            La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco.
            <a href="http://addicted.com.mx/terminosycondiciones" target="_blank"><b>Términos y condiciones de uso del Programa ADDICTED</b></a> |
            <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank"><b>Aviso de Privacidad</b></a>
        </div>
    </div>
</div>