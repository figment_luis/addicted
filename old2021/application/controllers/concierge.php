<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Concierge extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $response['status'] = false;
		$response['msg'] = "�Acceso no autorizado!";
		echo json_encode($response);
	}


	public function get_service()
	{
		$response['status'] = false;
		$response['msg'] = "Acceso no autorizado";

		if(strtolower($_SERVER['REQUEST_METHOD']) != "get") {
			$response['msg'] = "Medio no autorizado.";
			echo json_encode($response);
			return false;
		}
		if(!isset($_GET["url"]) || empty($_GET) || count($_GET)>1){
			$response['msg'] = "Datos incorrectos.";
			echo json_encode($response);
			return false;
		}

		$url_service = trim($_GET["url"]);
		$url_service = str_replace("@", "+", $url_service);
		$res = $this->get_curl_service($url_service);
		echo $res;
	}


	function get_curl_service($url)
	{
		$response['status'] = false;
		$response['msg'] = "Acceso no autorizado...";

		if(strlen($url) <= 30){ $response['msg']="Servicio no reconocido (1)."; return json_encode($response); }
		if(stripos($url, "oncierge.addicted.com.mx") > 0) { $response['msg']="Servicio no reconocido (2)."; return json_encode($response); }
		$url = "http://concierge.addicted.com.mx/services/" . $url;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$remote_server_output = curl_exec ($ch);
		curl_close ($ch);
		return $remote_server_output;
	}
 
	
}