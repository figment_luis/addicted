<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Descuentos extends CI_Controller {

	public function index()
	{
        $data['descuentos'] = $this->contenido->get_all('descuentos');
        $this->load->view('header');
        $this->load->view('descuentos',$data);
        $this->load->view('footer');
	}
 
	
}