<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuenta extends CI_Controller {

	public function index()
	{
        if(isset($_COOKIE['account'])){
            $this->load->view('header');
            $data['account'] =   json_decode($_COOKIE['account'], true);
            $this->load->view('cuenta', $data);
            $this->load->view('footer');
        }else{
            redirect(base_url());
        }        
    }
    
    public function setCookie(){
        $this->load->helper('cookie');
        $datauser = isset($_POST['datauser']) ? $_POST['datauser'] : NULL;
        set_cookie('account', $_POST['datauser'], time()+1800);
    }
  
    public function logout(){
        $this->load->helper('cookie');
        delete_cookie('account');
        redirect(base_url());
    }
 
	
}