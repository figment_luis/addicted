<!-- Modal Login -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">

       <div class="text-center modal-title">
         <img src="<?= base_url('assets/img/logo_addicted_black.png'); ?>" width="150" height="64">
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  id="form-login" role="form" method="post" action="" autocomplete="false">
      <div class="modal-body">
        <h5>INICIAR SESIÓN</h5>
        <br>

            <div class="form-group">
              <label for="mailuser">Email</label>
              <input type="email" class="form-control" id="mailuser" name="mailuser" aria-describedby="emailHelp" placeholder="Enter email"  autocomplete="off" required>
            </div>
            <div class="form-group">
              <label for="password">Contraseña</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña"  autocomplete="off" required>
            </div>
            <p class="text-right">
                <a href="#password" data-toggle="modal" data-target="#modalPassword" class="btn-recovery">Recuperar Contraseña</a>
            </p>
            <div  class="form-text msg-error  text-danger"></div>

            <h4 class="msg-correct"><i class="fa fa-check text-success"></i> Redireccionando ...<br></h4>
      </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-form" id="btn-login">INICIAR SESIÓN</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal Recuperar Contraseña -->
<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="modalPassword" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">

       <div class="text-center modal-title">
         <img src="<?= base_url('assets/img/logo_addicted_black.png'); ?>" width="150" height="64">
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form  id="form-recovery" role="form" method="post" action="">
      <div class="modal-body">
        <h5>RECUPERAR CONTRASEÑA</h5>
        <br>
            <div class="form-group">
              <label for="mailrecovery">Email</label>
              <input type="email" class="form-control" id="mailrecovery" name="mailrecovery" aria-describedby="emailHelp" placeholder="Enter email" required>
            </div>
            <br>
            <div  class="form-text msg-error  text-danger"></div>
            <h4 class="msg-correct"><i class="fa fa-check text-success"></i> Gracias en breve recibirá un correo para restablecer su contraseña.<br></h4>
      </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-success btn-form" id="btn-recovery">RECUPERAR CONTRASEÑA</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- Modal Registro -->
<div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="modalLogin" aria-hidden="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">

       <div class="text-center modal-title">
         <img src="<?= base_url('assets/img/logo_addicted_black.png'); ?>" width="150" height="64">
        </div>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-registro" role="form" method="post" action="">
      <div class="modal-body">
        <h5>PRE REGÍSTRATE</h5>
            <div class="form-group">
              <label for="mailuserR">Email</label>
              <input type="email" class="form-control" id="mailuserR" name="mailuserR"  aria-describedby="emailHelp" placeholder="Enter email" required>

            </div>

            <div class="form-group">
              <label for="firstname">Nombres</label>
              <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Nombres" required>
            </div>
            <div class="form-group">
              <label for="lastname1">Apellido Paterno</label>
              <input type="text" class="form-control" id="lastname1" name="lastname1" placeholder="Apellido Paterno" required>
            </div>
            <div class="form-group">
              <label for="lastname2">Apellido Materno</label>
              <input type="text" class="form-control" id="lastname2" name="lastname2" placeholder="Apellido Materno" required>
            </div>
            <div class="form-check">
              <input type="checkbox" class="form-check-input" id="privacidad" required>
              <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank" class="form-check-label" for="privacidad">Acepto Aviso de Privacidad</a>
            </div>
            <br>
            <div  class="form-text msg-error  text-danger"></div>
            <h4 class="msg-correct"><i class="fa fa-check text-success"></i> Gracias por completar el formulario, acuda al módulo de concierge dentro Antara Fashion Hall para recibir su tarjeta.<br></h4>
      </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-success" id="btn-registro">ENVIAR DATOS</button>
        <button type="button" class="btn btn-danger btn-form" data-dismiss="modal">CERRAR</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div id="promo" style="width: 600px; height: 100%; display:none;">
  <?php if($this->config->item('popup_micrositio_link')): ?>
    <a href="<?php echo base_url($this->config->item('popup_micrositio_link')); ?>" target="_blank" class="btnPop_facebook">
      <img src="<?php echo base_url($this->config->item('popup_micrositio_desktop_img')); ?>" border="0" style="width: 100%; height: 100%;" />
    </a>
  <?php else: ?>
    <img src="<?php echo base_url($this->config->item('popup_micrositio_desktop_img')); ?>" border="0" style="width: 100%; height: 100%;" />
  <?php endif; ?>
</div>
<div id="promo_mobile" style="width: 600px; height: 100%; display: none;">
  <?php if($this->config->item('popup_micrositio_link')): ?>
    <a href="<?php echo base_url($this->config->item('popup_micrositio_link')); ?>" target="_blank" class="btnPop_facebook">
      <img src="<?php echo base_url($this->config->item('popup_micrositio_desktop_img')); ?>" border="0" style="width: 100%; height: 100%;" />
    </a>
  <?php else: ?>
    <img src="<?php echo base_url($this->config->item('popup_micrositio_desktop_img')); ?>" border="0" style="width: 100%; height: 100%;" />
  <?php endif; ?>
</div>

<script>

    var popupvisible = <?php echo ((date("Y-m-d H:i:s") >= $this->config->item('popup_micrositio_start') && date("Y-m-d H:i:s") <= $this->config->item('popup_micrositio_end')))? "true":"false"; ?>;
    var isMobile = false;


    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
      isMobile = true;
    }

    // Condition PopUp Mobile
    if(popupvisible){
      if(isMobile == false){
        setTimeout(function(){
          $.fancybox.open('#promo');
        }, 1000);
      }else {
        setTimeout(function(){
          $.fancybox.open('#promo_mobile');
        }, 1000);
      }
    }
</script>


<script>var base_url = '<?php echo base_url(); ?>'</script>

<script src="<?= base_url('assets/js/account.js'); ?>"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-104213024-1', 'auto');
  ga('send', 'pageview');

</script>




</body>
</html>
