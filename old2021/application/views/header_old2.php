<!DOCTYPE html>
    <html lang="es">
<?php
if(isset($_SERVER['HTTPS']))  "<!-- HTTPS -->";
echo "<!-- ".$_SERVER['SERVER_NAME']." -->";
?>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">

        <title>Addicted</title>


        <meta property="og:title" content="Addicted"/>
        <meta property="og:image" content="http://addicted.com.mx/assets/img/logo_addicted.png"/>
        <meta property="og:url" content="http://addicted.com.mx"/>

        <meta name="apple-itunes-app" content="app-id=635100637">
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png') ?>">
        <link rel="icon" type="image/png" href="<?= base_url('assets/img/favicon.png') ?>" />
        <link rel="shortcut icon" sizes="196x196" href="<?php echo base_url('assets/img/icono.png') ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/icono.png') ?>" />



        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('assets/css/style.css?v=').mt_rand(); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/jquery.fancybox.css').mt_rand(); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="http://malsup.github.com/jquery.cycle2.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?= base_url('assets/js/script.js?v=').mt_rand(); ?>"></script>
        <script src="<?= base_url('assets/js/sha1.js'); ?>"></script>
        <script src="<?= base_url('assets/js/pdf417-min.js'); ?>"></script>
        <script src="<?= base_url('assets/js/jquery.fancybox.js'); ?>"></script>



    </head>

    <body>
    <?php
        $controller = $this->router->class;
        $class = 'class="bg-home"';
        if($controller != 'home'){
            $class =  'class="bg-interiores"';
        }
    ?>
    <input type="hidden" value="<?= base_url(); ?>">
        <?php
            $class = 'class="bg-home"';
            if($controller != 'home'){
                $class =  'class="bg-interiores"';
            }
        ?>

        <header <?= $class ?>>
            <div id="nav-icon3" class="button-menu"><span></span><span></span><span></span><span></span></div>
            <div class="buttons-loyality">
                <!-- <a href="#preregistro" data-toggle="modal" data-target="#modalRegistro">PRE REGÍSTRATE</a>
                <label> | </label>             -->
                <a href="<?= base_url('cuenta'); ?>" class="link-cuenta <?php echo isset($_COOKIE['account']) ? '' : 'no-display'; ?>">ESTADO DE CUENTA</a>
                <a href="#login"  data-toggle="modal" data-target="#modalLogin" class="link-login <?php echo isset($_COOKIE['account']) ? 'no-display' : ''; ?>">INICIAR SESIÓN</a>
            </div>
        </header>

        <div class="menu-content">
            <ul class="menu-list">
                <li><a class="black" href="<?= base_url('home'); ?>">INICIO</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('descubre'); ?>">DESCUBRE ADDICTED</a>
                    <div id="lineMenu"></div>
                </li>
                <li class="hidden"><a class="black no-events" href="#">PREMIOS Y BENEFICIOS</a></li>
                <li><a class="black" href="<?= base_url('certificados'); ?>">CERTIFICADOS DE REGALO</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('descuentos'); ?>">DESCUENTOS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('beneficios'); ?>">BENEFICIOS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('experiencias'); ?>">EXPERIENCIAS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('contacto'); ?>">CONTACTO</a>
                    <div id="lineMenu"></div>
                </li>
            </ul>
        </div>
