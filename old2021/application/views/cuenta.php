<div class="interiores">
    <div class="block-slide">


        <div class="cycle-slideshow" data-cycle-timeout="3000" data-cycle-slides="> div">
            <div style="background-image:url(<?= base_url('assets/img/backgrounds/contacto.png') ?>);" class="img-responsive img-slide">
                <p>
            </div>
        </div>

        <div class="clearfix"></div>

        <img class="logo-slide" src="<?= base_url('assets/img/logo_addicted.png') ?>" alt="Addicted">
        <div class="title-content-xs visible-xs">
            <hr>
            <h1 class="light">ESTADO DE CUENTA</h1>
        </div>
    </div>
    <div class="block-content descubre">
        <div class="contacto">

            <div class="title-content hidden-xs text-center">ESTADO DE CUENTA</div>               
                <div class="text-center info-contacto">
                <h2>SOCIO: <?= $account['addicted']['level_name']; ?></h2>
                <br>
                <p class="text-dark">CLIENTE:</p>
                <p><b><?= $account['user'][0]['firstname'] . ' ' . $account['user'][0]['lastname'];  ?></b></p>
                <br>
                <p>No. Tarjeta:</p>
                <p><?= $account['addicted']['card']; ?></p>
                <br>
                <p>Tarjeta digital:</p>
                <div id="code-digital">                
                </div>
                <br>
                <h3>Puntos Firmes:</h3>
                <h2><?= number_format($account['addicted']['points']); ?></h2>
                <br>
                <a href="<?= base_url('cuenta/logout') ?>" class="btn btn-logout" role="button"><i class="fa fa-times"></i> CERRAR SESIÓN</a>
            </div> 

            <div class="footer-interiores">
                La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco.
                <a href="http://addicted.com.mx/terminosycondiciones" target="_blank"><b>Términos y condiciones de uso del Programa ADDICTED</b></a> |
                <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank"><b>Aviso de Privacidad</b></a>
            </div>
        </div>
    </div>
</div>

<script>
    PDF417.init('<?= $account['addicted']['card']; ?>');
       var barcode = PDF417.getBarcodeArray();
       var bw = 2;
       var bh = 2;
       var canvas = document.createElement('canvas');
       canvas.width = bw * barcode['num_cols'];
       canvas.height = bh * barcode['num_rows'];
       document.getElementById('code-digital').appendChild(canvas);
       var ctx = canvas.getContext('2d');
       var y = 0;
       for ( var r = 0; r < barcode['num_rows']; ++r) {
         var x = 0;
         for ( var c = 0; c < barcode['num_cols']; ++c) {
           if (barcode['bcode'][r][c] == 1) {
             ctx.fillRect(x, y, bw, bh);
           }
           x += bw;
         }
         y += bh;
       }
</script>