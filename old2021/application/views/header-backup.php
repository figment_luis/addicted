<?php
 $controller = $this->router->class;
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <meta charset="UTF-8">  
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
       
        <title>Addicted</title>
       
        
        <meta property="og:title" content="Addicted"/>
        <meta property="og:image" content="http://addicted.com.mx/assets/img/logo_addicted.png"/>
        <meta property="og:url" content="http://addicted.com.mx"/>
        
        <meta name="apple-itunes-app" content="app-id=635100637">
        <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png') ?>">
        <link rel="icon" type="image/png" href="<?= base_url('assets/img/favicon.png') ?>" />
        <link rel="shortcut icon" sizes="196x196" href="<?php echo base_url('assets/img/icono.png') ?>">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/img/icono.png') ?>" />
 
     
       
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('assets/css/style.css'); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="http://malsup.github.com/jquery.cycle2.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?= base_url('assets/js/script.js'); ?>"></script>
    </head>

    <body>
    <input type="hidden" value="<?= base_url(); ?>">
<!--
        <div id="fade-in">
            <div><i class="fa fa-spinner fa-spin"></i></div>
        </div>
-->
        <?php
            $class = 'class="bg-home hidden-xs"';
            if($controller != 'home'){
                $class =  'class="bg-interiores hidden-xs"';
            }

        ?>
        <header <?= $class ?>>
            <div id="nav-icon3" class="button-menu"><span></span><span></span><span></span><span></span></div>
            <div class="block-login">
                <div class="form-inline">
                    <label for="" id="openLogin">LOG IN</label>
                    <div class="form-group">
                        <label class="sr-only" for="email">@correo electrónico</label>
                        <input type="email" class="form-control" id="email" placeholder="@correo electrónico" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">contraseña</label>
                        <input type="password" class="form-control" id="password" placeholder="contraseña" required>
                    </div>

                    <button  class="btn btn-default btn-login">Aceptar</button>
                </div>
            </div>
        </header>
        <header class="visible-xs header-mobile bg-home">
            <div id="nav-icon3" class="button-menu-mobile"><span></span><span></span><span></span><span></span></div>
            <label id="openLoginMobile">LOG IN</label>
        </header>
        <div class="block-login-mobile visible-xs text-center">
           <img src="<?= base_url('assets/img/logo_addicted_black.png'); ?>" width="150" height="64">
           <br>
           <br>
           <br>
            <div class="form-inline">            
              <div class="form-group">
                <label class="sr-only" for="email">@correo electrónico</label>
                <input type="email" class="form-control" id="email" placeholder="@correo electrónico" required>
              </div>
              <div class="form-group">
                <label class="sr-only" for="password">contraseña</label>
                <input type="password" class="form-control" id="password" placeholder="contraseña" required>
              </div>
 
              <button  class="btn btn-default btn-block btn-login">Aceptar</button>
            </div>
        </div>
        <div class="side-menu-left side-menu-mobile">
            <ul class="menu-list">
                <li><a class="black" href="<?= base_url('home'); ?>">INICIO</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('descubre'); ?>">DESCUBRE ADDICTED</a>
                    <div id="lineMenu"></div>
                </li>
                <li class="hidden"><a class="black no-events" href="#">PREMIOS Y BENEFICIOS</a></li>
                <li><a class="black" href="<?= base_url('certificados'); ?>">CERTIFICADOS DE REGALO</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('descuentos'); ?>">DESCUENTOS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('beneficios'); ?>">BENEFICIOS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('experiencias'); ?>">EXPERIENCIAS</a>
                    <div id="lineMenu"></div>
                </li>
                <li><a class="black" href="<?= base_url('contacto'); ?>">CONTACTO</a>
                    <div id="lineMenu"></div>
                </li>
            </ul>
        </div>