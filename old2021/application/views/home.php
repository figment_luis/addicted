
<!-- Popup -->
<div id="promo" style="width:100%;display:none;">
    <img src="<?php echo base_url('assets/img/popup/abierto_desktop.jpg')."?v=".rand(); ?>"/>
</div>
<div id="promo_mobile" style="height: 100%; display: none;">
    <img src="<?php echo base_url('assets/img/popup/abierto_mobile.png')."?v=".rand(); ?>" style="width: 100%; object-fit: contain" />
</div>

<div class="home">
    <div class="row">
        <div class="col-12 col-lg-5 col-md-5  col-sm-4 no-float no-padding bg-descubre overlay efecto">
            <a href="<?= base_url('descubre'); ?>" id="overlay"></a> <img src="assets/img/logo_addicted.png" class="logo-addicted img-responsive" alt="Addicted">
            <div class="block-text-descubre-home animate-text">
                <hr>
                <h1 class="light">DESCUBRE</h1>
                <p class="light">Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios y beneficios.</p>
            </div>
            <div class="footer-home hidden">
                La página de Antara es una obra creativa amparada por las leyes de potección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara Polanco. Términos y condiciones de uso del Programa ADDICTED | <a href="<?= base_url('avisoprivacidad'); ?>">Aviso de Privacidad</a>
            </div>
        </div>
        <div class="col-12 col-lg-4 col-md-4 col-sm-4  no-float no-padding block-pares">
            <div class="block-50h bg-premios overlay efecto">
                <a href="<?= base_url('certificados'); ?>" id="overlay"></a>
                <div class="block-text-premios-home animate-text">
                    <hr>
                    <h1 class="light">PREMIOS</h1>
                    <p class="light">Addicted te premia con descuentos,<br>certificados y experiencias.</p>
                </div>
            </div>
            <div class="block-50h bg-contacto overlay efecto">
                <a href="<?= base_url('contacto'); ?>" id="overlay"></a>
                <div class="block-text-premios-home animate-text">
                    <hr>
                    <h1 class="light">CONTACTO</h1>
                    <p class="light">Será un placer comunicarnos contigo.</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 col-md-3 col-sm-4   no-float no-padding bg-beneficios overlay block-unico efecto">
            <a href="<?= base_url('beneficios'); ?>" id="overlay"></a>
            <div class="block-text-beneficios-home animate-text">
                <hr class="animated fadeIndRight">
                <h1 class="light">BENEFICIOS</h1>
                <p class="light">Antara te ofrece estos beneficios sólo por ser parte de addicted.</p>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
        var popupvisible = false;
        var isMobile = false; //initiate as false

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
            isMobile = true;
        }

        // Condition PopUp
        if(popupvisible){
            if(isMobile == false){
                setTimeout(function(){
                    $.fancybox.open('#promo');
                }, 1000);
            }else {
                setTimeout(function(){
                        $.fancybox.open('#promo_mobile');
                }, 1000);
            }
        }
    })
</script>
