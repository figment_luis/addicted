<div class="interiores">
    <div class="block-slide">


        <div class="cycle-slideshow" data-cycle-timeout="3000" data-cycle-slides="> div">
            <div style="background-image:url(<?= base_url('assets/img/backgrounds/contacto.png') ?>);" class="img-responsive img-slide">
                <p>
            </div>
        </div>

        <div class="clearfix"></div>

        <img class="logo-slide" src="<?= base_url('assets/img/logo_addicted.png') ?>" alt="Addicted">
        <div class="title-content-xs visible-xs">
            <hr>
            <h1 class="light">CONTACTO</h1>
<!--            <p class="light">Addicted es el plan de lealtad de Antara Fashion Hall que te da los mejores premios y beneficios.</p>-->
        </div>
    </div>
    <div class="block-content descubre">
        <div class="contacto">
            <div class="title-content hidden-xs text-center">CONTACTO</div>
            <div class="text-center"><img src="<?= base_url('assets/img/logo_contacto.png') ?>" alt="Addicted" style="margin-top: 20px"></div>
            <div class="text-center info-contacto" style="margin-top: 20px"> <!--Modificacion de Style hecha para evitar dejar tanto espacio que dejo el nuevo logo sin el texto FashionHall-->
                Nuestro Concierge está ubicado en Planta Baja
                <br><br>Dirección:<br>
                <br> Av. Éjercito Nacional no.843 col.Granada Delegación Miguel Hidalgo C.P. 11520 Distrito Federal Zona Polanco
                <br>
                <br> Centro de atención a clientes:
                <h2><a href="tel:45938870">4593-8870</a></h2>
            </div>

            <div class="info-antara">
                <p>
                    Antara ofrece un espacio único al aire y un equilibrio entre belleza, glamour y estilo donde comprar se convierte en una experiencia única.    
                </p>
                <p>
                    Antara busca brindar la más alta calidad de vida, con un paseo peatonal como reflejo de las mejores visiones de una calle armoniosa, segura y atractiva.
                </p>
                <p>
                    Antara es un shopping center exclusivo donde se encuentran las tiendas embajadas de la última moda y tendencias para hombre, mujer y ropa de bebé. Como en un fashion show, en Antara se encuentran tiendas para comprar ropa formal o casual, vestidos cortos, jeans, etc., Zara, Bershka, Adidas, Armani Exchange, Calvin Klein, Michael Kors. También es un excelente lugar para comprar joyería de plata, oro, relojes, anillos de compromiso y todo tipo de bisutería.
                </p>
                <p>
                    Para los amantes del diseño de interiores, tenemos tiendas como Casa Palacio, Zara Home, Otilia Freyre Tilita, etc.               
                </p>
                <p>
                    Sabemos que no todo es moda, vestidos y decoración, y por eso Antara ofrece los mejores restaurantes y bares de la ciudad, como lo son La 20, Odeon, Syrah, La Cabrera, etc.
                </p>
            </div>
            <div class="footer-interiores">
                La página de Antara es una obra creativa amparada por las leyes de protección de la propiedad intelectual, así como todos los elementos que la componen y es propiedad exclusiva de Antara.
                <a href="http://addicted.com.mx/terminosycondiciones" target="_blank"><b>Términos y condiciones de uso del Programa ADDICTED</b></a> |
                <a href="http://addicted.com.mx/avisodeprivacidad" target="_blank"><b>Aviso de Privacidad</b></a>
            </div>
        </div>
    </div>
</div>