<?php

    
    function setBoldText($text,$delim)
    {
        $pos = strpos($text, $delim);
        $textbold = substr($text, $pos);
        $textnormal = substr($text, 0,$pos);
        return $textnormal.' <span style="color:#212020;font-weight: bold;">'.$textbold.'</span>';
    }
    
   
?>