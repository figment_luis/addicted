<?php if(!defined('BASEPATH'))exit('No direct script access allowed');

class services extends CI_Model
{
	public function get_experiencias($value){
        
        if ($value == null){
            $value = 'puntos';
        }

        $this->db->select('*');
        $this->db->from('experiencias');  
        $this->db->where('enabled',1);
        $this->db->order_by($value, "asc"); 
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_obsequios($value){
        
        if ($value == null){
            $value = 'puntos';
        }

        $this->db->select('*');
        $this->db->from('obsequios');  
        $this->db->where('enabled',1);
        $this->db->order_by($value, "asc"); 
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_beneficios(){
        $this->db->select('*');
        $this->db->from('beneficios');  
        $this->db->where('enabled',1);
        $this->db->order_by("title", "asc"); 
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_descuentos(){
        $this->db->select('*');
        $this->db->from('descuentos');  
        $this->db->where('enabled',1);
        $this->db->order_by("title", "asc"); 
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_descubre(){
        $this->db->select('*');
        $this->db->from('descubre');  
        $query = $this->db->get();

        return $query->result_array();
    }



    public function preregister($data)
    {
        date_default_timezone_set('America/Mexico_City');

        $user['firstname'] = $data['name'];
        $user['lastname1'] = $data['lastname'];
        $user['lastname2'] = $data['lastnamem'];
        $user['email'] = $data['email'];
        $user['email_alternativo'] = $data['email'];
        $user['news'] = 'ok';
        $user['added'] = date('Y-m-d H:i:s');
 
        $query = $this->db->query("SELECT COUNT(*) as total FROM preregistro WHERE  email = '" . $user['email'] . "'");

        $row = $query->row();

        if ($row->total == 0) {
            if ($this->db->insert('preregistro', $user)) {
                $record_id = $this->db->insert_id();

                $status['status'] = true;
                $status['msg'] = "correcto";

                $this->load->model('sendmails');
                $this->sendmails->mailPreregister($data['email'],$data['name']);

                return $status;
            } else {
                $status['status'] = false;
                $status['msg'] = "Encontramos un error en nuestro servicio, intentelo más tarde.";
                return $status;
            }
        } else {
            $status['status'] = false;
            $status['msg'] = "Otra persona ya está usando este e-mail.";
            return $status;
        }


    }

}

?>