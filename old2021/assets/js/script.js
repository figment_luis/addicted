$(document).ready(function () {
    // Swhow more on content
    // Configure/customize these variables.
    var showChar = 180;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Ver más";
    var lesstext = "Cerrar";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    }); 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    
    
    
    // Effect button menu
    $('.button-menu').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('open');
        $(".menu-content").toggleClass('open-menu');        
    });
    
    //Efecto button menu Mobile{
     $(".button-menu-mobile").click(function(){
         var menu = $(".side-menu-mobile").css('top');
         var login = $(".block-login-mobile").css('top'); 
         var timeout = 0;
         
         $(this).toggleClass('open');
         
         if(login == '50px'){
             timeout = 400;
             $(".block-login-mobile").animate({
                opcity: 0,
                top: '-350px'
            },300); 
         }
         setTimeout(function(){
              if(menu == '-380px'){
                 $(".side-menu-mobile").animate({
                    top: 0      
                 },300);
             }
             else{
                $(".side-menu-mobile").animate({
                    top: "-380px"      
                 },300);  
             }
         }, timeout);

     });

    
    //Effect Login Desktop
    $(".block-login .form-group").fadeOut(200);
    $(".block-login .btn-default").fadeOut(200);
    $("#openLogin").click(function(e){
       e.preventDefault();
       var position = $(".block-login").css('right');       
        if(position == '10px'){
            $(".block-login .form-group").fadeOut(200);
            $(".block-login .btn-default").fadeOut(200);
            $(".block-login").animate({
                right: '-450px'
            },300);
        }else{
            $(".block-login .form-group").fadeIn(200);
            $(".block-login .btn-default").fadeIn(200);
           $(".block-login").animate({
                right: '10px'
            },300); 
        }
    });
    
    //Effecto Login Mobile
    $("#openLoginMobile").click(function(){
        var position = $(".block-login-mobile").css('top'); 
        var menu = $(".side-menu-mobile").css('top');
        var timeout = 0;
        if(menu == '0px'){
            timeout = 400;
            $(".side-menu-mobile").animate({
                top: "-380px"      
             },300);  
        }
        setTimeout(function(){
        if(position == '-350px'){
            $(".block-login-mobile").animate({
                opcity: 1,
                top: '50px'
            },300);
        }
        else{
            $(".block-login-mobile").animate({
                opcity: 0,
                top: '-350px'
            },300); 
        }  
        },timeout);

    })
    
   
    // Effect home  & Loading Out
//    setTimeout(function(){
//        $('#fade-in').fadeOut(300);
//        $( ".efecto" ).each(function( index ) {
//          $(this).addClass('animated zoomIn');
//        });
//    },3000);
    
    
    // Effect text home   
    $(".overlay").mouseover(function(e){
        var line = $(this).find('hr');
        var text = $(this).find('p');
        line.removeClass('animated '+' fadeOutRight');        
        line.removeClass('animated '+' fadeInRight');
        text.removeClass('animated '+' fadeInUp');
        text.removeClass('animated '+' fadeOutDown');
        setTimeout(function(){
           line.addClass('animated fadeInRight');
           text.addClass('animated fadeInUp');
        },1000);
        line.addClass('animated fadeInRight');
        text.addClass('animated fadeInUp');
        
    }).mouseout(function(e){
        var line = $(this).find('hr');
        var text = $(this).find('p');
        line.addClass('animated fadeOutRight');
        text.addClass('animated fadeOutDown');
    });
    
    //LOGIN
    $(".btn-login").click(function () {
        var email  = $(this).parent().find('#email');
        var password  = $(this).parent().find('#password');
     
        var data = {
            mail: email.val(),
            password: password.val()
        };

        $.post("http://concierge.addicted.com.mx/addicted/login", data, function (res) {
            console.log("success");
            if (res == 1) {
                console.log('Usuaro y/o contreaseña incorrectos');
            } else if (res == 2) {
                console.log('Error al validar formulario desde servidor.');
            } else if (res == 3) {
                console.log('Todo correcto');
                $('#top-input-email').val('');
                $('#top-input-password').val('');;
                window.location.href = "http://concierge.addicted.com.mx/web/usuario";
            }
        })
    });
    
});