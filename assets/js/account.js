  $('#form-registro').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var data  = $(this).serializeArray();
    var textButton = $(this).find('.btn-success').html();

    $(this).find('.btn-success').addClass('disabled');
    $(this).find('.btn-success').html('<i class="fa fa-spinner fa-spin fa-fw"></i> PROCESANDO...');

    var url = "https://addicted.com.mx/services/preregister/user/"+data[1]['value'].trim()+"/"+data[2]['value'].trim()+"/"+data[3]['value'].trim()+"/"+data[0]['value'].replace('@','+').trim()+"";

    get_url(url, '#form-registro', textButton);

    return false;
  });

  $('#form-login').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var data  = $(this).serializeArray();
    var textButton = $(this).find('.btn-success').html();

    $(this).find('.btn-success').addClass('disabled');
    $(this).find('.btn-success').html('<i class="fa fa-spinner fa-spin fa-fw"></i> PROCESANDO...');

	var url = "get_login/check/"+data[0]['value'].trim()+"/"+CryptoJS.SHA1( data[1]['value'].trim());

	get_service_concierge(url, '#form-login', textButton);

    return false;
  });

  $('#form-recovery').submit(function(e) {
    e.preventDefault();
    e.stopPropagation();

    var data  = $(this).serializeArray();
    var textButton = $(this).find('.btn-success').html();

    $(this).find('.btn-success').addClass('disabled');
    $(this).find('.btn-success').html('<i class="fa fa-spinner fa-spin fa-fw"></i> PROCESANDO...');

    var url = "get_passwordRecovery/RequestRecovery/"+data[0]['value'].trim();

    get_service_concierge(url, '#form-recovery', textButton);

    return false;
  });


  $(".btn-danger").click(function(){
    resetForms();
  });


  function resetForms(){
      $('#form-login')[0].reset();
      $('#form-registro')[0].reset();
      $('#form-recovery')[0].reset();
      $('.msg-error').fadeOut();
  }

  function get_url($url, $form, $textButton){
    console.log("send:", $url);
    $.ajax({
            type: 'GET',
            url: $url,
            data: "",
            success: function(data){
              var data = JSON.parse(data);
                if(data['status'] == true){
					  $('.modal-footer').css('display', 'none');
                      $($form).find('.msg-correct').fadeIn();
                      $($form).find('.msg-error').fadeOut();
                      $($form).find('.btn-success').removeClass('disabled').html($textButton);
                      if($form == '#form-login'){
                        $.post(base_url+'cuenta/setCookie', {datauser: JSON.stringify(data)}, function(data){  });
                        setTimeout(function(){
                          window.location = base_url+'cuenta';
                          resetForms();
                        }, 2000);
                      }

                      resetForms();
                }else{
                  $($form).find('.msg-error').html('<i class="fa fa-times"></i> '+data['msg']).fadeIn();
                  $($form).find('.btn-success').removeClass('disabled').html($textButton);
                }
                return false;
            },
            error: function(xhr, type, exception) {
                resetForms();
                return false;
            }
        });
  }


  function get_service_concierge($url, $form, $textButton){
    //console.log("send:", $url);
    $.ajax({
            type: 'GET',
            url: base_url + 'concierge/get_service/',
            data: "url="+encodeURIComponent($url),
            success: function(data){
              var data = JSON.parse(data);
                if(data['status'] == true){
					  $('.modal-footer').css('display', 'none');
                      $($form).find('.msg-correct').fadeIn();
                      $($form).find('.msg-error').fadeOut();
                      $($form).find('.btn-success').removeClass('disabled').html($textButton);
                      if($form == '#form-login'){
                        $.post(base_url+'cuenta/setCookie', {datauser: JSON.stringify(data)}, function(data){  });
                        setTimeout(function(){
                          window.location = base_url+'cuenta';
                          resetForms();
                        }, 2000);
                      }

                      resetForms();
                }else{
                  $($form).find('.msg-error').html('<i class="fa fa-times"></i> '+data['msg']).fadeIn();
                  $($form).find('.btn-success').removeClass('disabled').html($textButton);
                }
                return false;
            },
            error: function(xhr, type, exception) {
                resetForms();
                return false;
            }
        });
  }